package ru.appril.kraspg.presenter;

import android.annotation.SuppressLint;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.LoginContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.LoginService;
import ru.appril.kraspg.network.RetrofitService;

public class LoginPresenter implements LoginContact.Presenter {
    private LoginContact.View mView;
    private LoginService loginService = RetrofitService.create(LoginService.class);

    public LoginPresenter(LoginContact.View mView) {
        this.mView = mView;
    }

    @Override
    @SuppressLint("CheckResult")
    public void Auth(String email, String password, String device) {
        mView.setEnable(false);
        loginService.Auth(email, password, device).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    mView.setEnable(true);

                    if (result.getError().getCode() == 0)
                        mView.Auth(result.getUser());
                    else
                        mView.showError(result.getError());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.setEnable(true);
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }

    @Override
    public void Reg(String email, String password, String device) {
        mView.setEnable(false);
        loginService.Reg(email, password, device).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    mView.setEnable(true);

                    if (result.getError().getCode() == 0)
                        mView.Reg(result.getUser());
                    else
                        mView.showError(result.getError());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.setEnable(true);
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }

    @Override
    public void deletePassword(String email) {
        mView.setEnable(false);
        loginService.deletePassword(email).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    mView.setEnable(true);

                    if (result.getError().getCode() == 0)
                        mView.showPassword(email);
                    else
                        mView.showError(result.getError());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.setEnable(true);
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }
}