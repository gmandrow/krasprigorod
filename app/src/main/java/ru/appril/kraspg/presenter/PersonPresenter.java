package ru.appril.kraspg.presenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.PersonContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.PasswordService;
import ru.appril.kraspg.network.RetrofitService;

public class PersonPresenter implements PersonContact.Presenter {
    private PersonContact.View mView;
    private PasswordService passwordService = RetrofitService.create(PasswordService.class);

    public PersonPresenter(PersonContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void changePassword(String old_password, String new_password) {
        passwordService.changePassword(old_password, new_password).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0)
                        mView.changePassword();
                    else
                        mView.showError(result.getError());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }
}