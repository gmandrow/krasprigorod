package ru.appril.kraspg.presenter;

import android.os.AsyncTask;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.PassengersContact;
import ru.appril.kraspg.dao.PassengersDao;
import ru.appril.kraspg.db.KpDatabaseFactory;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.PassengerService;
import ru.appril.kraspg.network.RetrofitService;

public class PassengersPresenter implements PassengersContact.Presenter {
    private PassengersContact.View mView;
    private PassengerService passengerService = RetrofitService.create(PassengerService.class);
    private PassengersDao passengersDao = KpDatabaseFactory.getInstance().getPassengersDao();

    public PassengersPresenter(PassengersContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void getPassengers() {
        mView.showProgress();
        passengerService.getPassengers().enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0) {
                        mView.hideProgress();
                        mView.showPassengers(result.getPassengers());
                        insertPassengersFromDb(result.getPassengers());
                    } else {
                        mView.hideProgress();
                        mView.showError(result.getError());
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.hideProgress();
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }

    @Override
    public void getPassengersFromDb() {
        AsyncTask.execute(() -> {
            List<Passenger> passengers = passengersDao.getPassengers();

            if (passengers.size() > 0) {
                mView.hideProgress();
                mView.showPassengers(passengers);
            }
        });
    }

    @Override
    public void insertPassengersFromDb(List<Passenger> passengers) {
        AsyncTask.execute(() -> {
            passengersDao.deletePassengers();
            passengersDao.insertPassengers(passengers);
        });
    }

    @Override
    public void deletePassengerFromDb(int passenger_id) {
        AsyncTask.execute(() -> passengersDao.deletePassenger(passenger_id));
    }

    @Override
    public void deletePassenger(int passenger_id) {
        passengerService.deletePassenger(passenger_id).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                deletePassengerFromDb(passenger_id);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }
}