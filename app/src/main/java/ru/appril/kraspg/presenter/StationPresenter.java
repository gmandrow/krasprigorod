package ru.appril.kraspg.presenter;

import android.os.AsyncTask;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.StationContact;
import ru.appril.kraspg.dao.StationsDao;
import ru.appril.kraspg.db.KpDatabaseFactory;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.model.Station;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.network.StationsService;

public class StationPresenter implements StationContact.Presenter {
    private StationContact.View mView;
    private StationsService stationsService = RetrofitService.create(StationsService.class);
    private StationsDao stationsDao = KpDatabaseFactory.getInstance().getStationsDao();

    public StationPresenter(StationContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void getStationsFrom() {
        mView.showProgress();

        stationsService.getStationsFrom().enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0) {
                        mView.hideProgress();

                        if (result.getStations().size() == 0)
                            mView.showError(new Error(0, "Станции не найнеды"));
                        else {
                            mView.hideError();
                            mView.showStations(result.getStations());
                            insertStationsFromDb(result.getStations());
                        }
                    } else {
                        mView.hideProgress();
                        mView.showError(result.getError());
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.hideProgress();
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }

    @Override
    public void getStationsFromDb() {
        AsyncTask.execute(() -> {
            List<Station> stations = stationsDao.getStations();

            if (stations.size() > 0) {
                mView.hideProgress();
                mView.hideError();
                mView.showStations(stations);
            }
        });
    }

    @Override
    public void insertStationsFromDb(List<Station> stations) {
        AsyncTask.execute(() -> stationsDao.insertStations(stations));
    }

    @Override
    public void getStationsTo(int from, String date) {
        mView.showProgress();

        stationsService.getStationsTo(from, date).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0) {
                        mView.hideProgress();

                        if (result.getStations().size() == 0)
                            mView.showError(new Error(0, "Станции не найнеды"));
                        else {
                            mView.hideError();
                            mView.showStations(result.getStations());
                        }
                    } else {
                        mView.hideProgress();
                        mView.showError(result.getError());
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.hideProgress();
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }
}