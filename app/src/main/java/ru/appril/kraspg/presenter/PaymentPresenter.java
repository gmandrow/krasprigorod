package ru.appril.kraspg.presenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.PaymentContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.PaymentService;
import ru.appril.kraspg.network.RetrofitService;

public class PaymentPresenter implements PaymentContact.Presenter {
    private PaymentContact.View mView;
    private PaymentService paymentService = RetrofitService.create(PaymentService.class);

    public PaymentPresenter(PaymentContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void getOrder(int from, int to, String date, int bags, int animals, int passenger_id) {
        paymentService.getOrder(from, to, date, bags, animals, passenger_id).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0)
                        mView.showOrder(result.getOrder());
                    else
                        mView.showError(result.getError());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }
}