package ru.appril.kraspg.presenter;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.ScheduleContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.network.ScheduleService;

public class SchedulePresenter implements ScheduleContact.Presenter {
    private ScheduleContact.View mView;
    private ScheduleService scheduleService = RetrofitService.create(ScheduleService.class);

    public SchedulePresenter(ScheduleContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void getSchedule(int from, int to, String date) {
        mView.showProgress();
        scheduleService.getSchedule(from, to, date).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0) {
                        mView.hideProgress();
                        mView.showSchedule(result.getSchedule());
                    } else {
                        mView.hideProgress();
                        mView.showError(result.getError());
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.hideProgress();
                mView.showError(new Error(0, "Ошибка подключения"));
                Log.d("mlg", t.toString());
            }
        });
    }
}