package ru.appril.kraspg.presenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.RobokassaContact;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.network.TicketService;

public class RobokassaPresenter implements RobokassaContact.Presenter {
    private RobokassaContact.View mView;
    private TicketService ticketService = RetrofitService.create(TicketService.class);

    public RobokassaPresenter(RobokassaContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void isPaymentOrder(int order_id) {
        ticketService.isPaymentOrder(order_id).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String result = response.body();

                if (result != null)
                    mView.isPaymentOrder(result);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
}