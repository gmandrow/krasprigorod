package ru.appril.kraspg.presenter;

import android.os.AsyncTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.OrderContact;
import ru.appril.kraspg.dao.OrdersDao;
import ru.appril.kraspg.db.KpDatabaseFactory;
import ru.appril.kraspg.model.Order;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.network.TicketService;

public class TicketPresenter implements OrderContact.Presenter {
    private OrderContact.View mView;
    private TicketService ticketService = RetrofitService.create(TicketService.class);
    private OrdersDao ordersDao = KpDatabaseFactory.getInstance().getOrdersDao();

    public TicketPresenter(OrderContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void getTicket(int order_id) {
        ticketService.getTicket(order_id).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0) {
                        mView.showTicket(result.getOrder());
                        insertOrderFromDb(result.getOrder());
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
            }
        });
    }

    @Override
    public void getOrderFromDb(int order_id) {
        AsyncTask.execute(() -> {
            Order order = ordersDao.getOrder(order_id);

            if (order != null)
                mView.showTicket(order);
        });
    }

    @Override
    public void insertOrderFromDb(Order order) {
        AsyncTask.execute(() -> ordersDao.insertOrder(order));
    }
}