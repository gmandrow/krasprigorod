package ru.appril.kraspg.presenter;

import android.os.AsyncTask;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.OrdersContact;
import ru.appril.kraspg.dao.OrdersDao;
import ru.appril.kraspg.db.KpDatabaseFactory;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Order;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.OrdersService;
import ru.appril.kraspg.network.RetrofitService;

public class OrdersPresenter implements OrdersContact.Presenter {
    private OrdersContact.View mView;
    private OrdersService ordersService = RetrofitService.create(OrdersService.class);
    private OrdersDao ordersDao = KpDatabaseFactory.getInstance().getOrdersDao();

    public OrdersPresenter(OrdersContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void getOrders() {
        mView.showProgress();
        ordersService.getOrders().enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    if (result.getError().getCode() == 0) {
                        mView.hideProgress();
                        mView.showOrders(result.getOrders());
                        insertOrdersFromDb(result.getOrders());
                    } else {
                        mView.hideProgress();
                        mView.showError(result.getError());
                    }
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.hideProgress();
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }

    @Override
    public void deleteOrder(int order_id) {
        ordersService.deleteOrder(order_id).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
            }
        });
    }

    @Override
    public void getOrdersFromDb() {
        AsyncTask.execute(() -> {
            List<Order> orders = ordersDao.getOrders();

            if (orders.size() > 0) {
                mView.hideProgress();
                mView.showOrders(orders);
            }
        });
    }

    @Override
    public void deleteOrderFromDb(int order_id) {
        AsyncTask.execute(() -> ordersDao.deleteOrder(order_id));
    }

    @Override
    public void insertOrdersFromDb(List<Order> orders) {
        AsyncTask.execute(() -> {
            ordersDao.deleteOrders();
            ordersDao.insertOrders(orders);
        });
    }
}