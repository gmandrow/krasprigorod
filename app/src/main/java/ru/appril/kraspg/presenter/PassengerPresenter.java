package ru.appril.kraspg.presenter;

import android.os.AsyncTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.PassengerContact;
import ru.appril.kraspg.dao.PassengersDao;
import ru.appril.kraspg.db.KpDatabaseFactory;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.model.Result;
import ru.appril.kraspg.network.PassengerService;
import ru.appril.kraspg.network.RetrofitService;

public class PassengerPresenter implements PassengerContact.Presenter {
    private PassengerContact.View mView;
    private PassengerService passengerService = RetrofitService.create(PassengerService.class);
    private PassengersDao passengersDao = KpDatabaseFactory.getInstance().getPassengersDao();

    public PassengerPresenter(PassengerContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void addPassenger(String full_name, String document_type, String document_number) {
        mView.setEnable(false);

        passengerService.addPassenger(full_name, document_type, document_number).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result = response.body();

                if (result != null) {
                    mView.setEnable(true);

                    if (result.getError().getCode() == 0) {
                        mView.showPassenger(result.getPassenger());
                        insertPassengerFromDb(result.getPassenger());
                    } else
                        mView.showError(result.getError());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                mView.setEnable(true);
                mView.showError(new Error(0, "Ошибка подключения"));
            }
        });
    }

    @Override
    public void insertPassengerFromDb(Passenger passenger) {
        AsyncTask.execute(() -> passengersDao.insertPassenger(passenger));
    }
}