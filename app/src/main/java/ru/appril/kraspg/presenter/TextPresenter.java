package ru.appril.kraspg.presenter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.appril.kraspg.contact.TextContact;
import ru.appril.kraspg.network.InfoService;
import ru.appril.kraspg.network.RetrofitService;

public class TextPresenter implements TextContact.Presenter {
    private TextContact.View mView;
    private InfoService infoService = RetrofitService.create(InfoService.class);

    public TextPresenter(TextContact.View mView) {
        this.mView = mView;
    }

    @Override
    public void getInfo(String info) {
        infoService.getInfo(info).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String result = response.body();

                if (result != null)
                    mView.showText(result);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
}