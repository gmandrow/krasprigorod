package ru.appril.kraspg.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Prefs {
    private Context context;

    private Prefs(Context context) {
        this.context = context;
    }

    public static Prefs init(Context context) {
        return new Prefs(context);
    }

    @SuppressWarnings("unchecked")
    public <T> T getValue(String key, T defaultValue) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        T resultValue = null;

        if (defaultValue instanceof Boolean)
            resultValue = (T) Boolean.valueOf(sPref.getBoolean(key, (Boolean) defaultValue));
        else if (defaultValue instanceof Float)
            resultValue = (T) Float.valueOf(sPref.getFloat(key, (Float) defaultValue));
        else if (defaultValue instanceof Integer)
            resultValue = (T) Integer.valueOf(sPref.getInt(key, (Integer) defaultValue));
        else if (defaultValue instanceof Long)
            resultValue = (T) Long.valueOf(sPref.getLong(key, (Long) defaultValue));
        else if (defaultValue instanceof String)
            resultValue = (T) sPref.getString(key, (String) defaultValue);

        return resultValue;
    }

    public <T> void setValue(String key, T value) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sPref.edit();

        if (value instanceof Boolean)
            editor.putBoolean(key, (Boolean) value);
        else if (value instanceof Float)
            editor.putFloat(key, (Float) value);
        else if (value instanceof Integer)
            editor.putInt(key, (Integer) value);
        else if (value instanceof Long)
            editor.putLong(key, (Long) value);
        else if (value instanceof String)
            editor.putString(key, (String) value);

        editor.apply();
    }

    public <T> T getModel(String key, Class<T> model) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        return new Gson().fromJson(sPref.getString(key, null), model);
    }

    public <T> void setModel(String key, T model) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        sPref.edit().putString(key, new GsonBuilder().create().toJson(model)).apply();
    }

    public void remove(String key) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove(key).apply();
    }

    public void clear() {
        PreferenceManager.getDefaultSharedPreferences(context).edit().clear().apply();
    }
}