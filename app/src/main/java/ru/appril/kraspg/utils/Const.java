package ru.appril.kraspg.utils;

public class Const {
    public static final String YANDEX = "9f5d2413-0d07-45ac-a4bf-3f795001048b";

    public static final int PREFS_DEFAULT_INT = 0;
    public static final String PREFS_DEFAULT_STRING = "";

    public static final String PREFS_USER_ID = "user_id";
    public static final String PREFS_TOKEN = "token";
    public static final String PREFS_EMAIL = "email";
    public static final String PREFS_SEARCH_FROM = "search_from";
    public static final String PREFS_SEARCH_TO = "search_to";
}
