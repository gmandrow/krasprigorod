package ru.appril.kraspg.contact;

import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Order;

public interface PaymentContact {

    interface View {

        void showOrder(Order order);

        void showError(Error error);
    }

    interface Presenter {

        void getOrder(int from, int to, String date, int bags, int animals, int passenger_id);
    }
}