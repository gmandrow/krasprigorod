package ru.appril.kraspg.contact;

public interface TextContact {

    interface View {

        void showText(String text);
    }

    interface Presenter {

        void getInfo(String info);
    }
}