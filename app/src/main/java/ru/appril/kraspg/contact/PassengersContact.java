package ru.appril.kraspg.contact;

import java.util.List;

import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Passenger;

public interface PassengersContact {

    interface View {

        void showPassengers(List<Passenger> passengers);

        void showError(Error error);

        void showProgress();

        void hideProgress();
    }

    interface Presenter {

        void getPassengers();

        void getPassengersFromDb();

        void insertPassengersFromDb(List<Passenger> passengers);

        void deletePassengerFromDb(int passenger_id);

        void deletePassenger(int passenger_id);
    }
}