package ru.appril.kraspg.contact;

import java.util.List;

import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Station;

public interface StationContact {

    interface View {

        void showStations(List<Station> stations);

        void showProgress();

        void hideProgress();

        void showError(Error error);

        void hideError();
    }

    interface Presenter {

        void getStationsFrom();

        void getStationsFromDb();

        void insertStationsFromDb(List<Station> stations);

        void getStationsTo(int from, String date);
    }
}