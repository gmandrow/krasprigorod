package ru.appril.kraspg.contact;

import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.User;

public interface LoginContact {

    interface View {

        void Auth(User user);

        void Reg(User user);

        void showPassword(String email);

        void showError(Error error);

        void setEnable(boolean enable);
    }

    interface Presenter {

        void Auth(String email, String password, String device);

        void Reg(String email, String password, String device);

        void deletePassword(String email);
    }
}