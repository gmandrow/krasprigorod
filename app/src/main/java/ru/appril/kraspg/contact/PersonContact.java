package ru.appril.kraspg.contact;

import ru.appril.kraspg.model.Error;

public interface PersonContact {

    interface View {

        void changePassword();

        void showError(Error error);
    }

    interface Presenter {

        void changePassword(String old_password, String new_password);
    }
}