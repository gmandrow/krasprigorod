package ru.appril.kraspg.contact;

import java.util.List;

import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Schedule;

public interface ScheduleContact {

    interface View {

        void showSchedule(List<Schedule> schedule);

        void showError(Error error);

        void showProgress();

        void hideProgress();
    }

    interface Presenter {

        void getSchedule(int from, int to, String date);
    }
}