package ru.appril.kraspg.contact;

import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Passenger;

public interface PassengerContact {

    interface View {

        void showPassenger(Passenger passenger);

        void showError(Error error);

        void setEnable(boolean enable);
    }

    interface Presenter {

        void addPassenger(String full_name, String document_type, String document_number);

        void insertPassengerFromDb(Passenger passenger);
    }
}