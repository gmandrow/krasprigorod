package ru.appril.kraspg.contact;

import ru.appril.kraspg.model.Order;

public interface OrderContact {

    interface View {

        void showTicket(Order order);
    }

    interface Presenter {

        void getTicket(int order_id);

        void getOrderFromDb(int order_id);

        void insertOrderFromDb(Order order);
    }
}