package ru.appril.kraspg.contact;

public interface RobokassaContact {

    interface View {

        void isPaymentOrder(String payment);
    }

    interface Presenter {

        void isPaymentOrder(int order_id);
    }
}