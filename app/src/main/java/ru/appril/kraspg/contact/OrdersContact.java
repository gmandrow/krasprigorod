package ru.appril.kraspg.contact;

import java.util.List;

import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Order;

public interface OrdersContact {

    interface View {

        void showOrders(List<Order> orders);

        void showError(Error error);

        void showProgress();

        void hideProgress();
    }

    interface Presenter {

        void getOrders();

        void deleteOrder(int order_id);

        void getOrdersFromDb();

        void deleteOrderFromDb(int order_id);

        void insertOrdersFromDb(List<Order> orders);
    }
}