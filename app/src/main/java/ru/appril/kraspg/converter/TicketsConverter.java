package ru.appril.kraspg.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;

import androidx.room.TypeConverter;
import ru.appril.kraspg.model.Ticket;

public class TicketsConverter {

    @TypeConverter
    public List<Ticket> getTickets(String tickets) {
        if (tickets == null)
            return null;
        else
            return Arrays.asList(new Gson().fromJson(tickets, Ticket[].class));
    }

    @TypeConverter
    public String saveTickets(List<Ticket> tickets) {
        if (tickets == null)
            return null;
        else
            return new GsonBuilder().create().toJson(tickets);
    }
}
