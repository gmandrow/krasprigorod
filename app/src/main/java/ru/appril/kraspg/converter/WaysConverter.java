package ru.appril.kraspg.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.List;

import androidx.room.TypeConverter;
import ru.appril.kraspg.model.Way;

public class WaysConverter {

    @TypeConverter
    public List<Way> getWays(String ways) {
        return Arrays.asList(new Gson().fromJson(ways, Way[].class));
    }

    @TypeConverter
    public String saveWays(List<Way> ways) {
        return new GsonBuilder().create().toJson(ways);
    }
}
