package ru.appril.kraspg.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import androidx.room.TypeConverter;
import ru.appril.kraspg.model.Passenger;

public class PassengerConverter {

    @TypeConverter
    public Passenger getPassenger(String passenger) {
        return new Gson().fromJson(passenger, Passenger.class);
    }

    @TypeConverter
    public String savPassenger(Passenger passenger) {
        return new GsonBuilder().create().toJson(passenger);
    }
}
