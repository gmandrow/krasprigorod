package ru.appril.kraspg.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.model.Ticket;

public class TicketsAdapter extends ArrayAdapter<Ticket> {
    @BindView(R.id.order_tickets_item_ticket)
    FrameLayout mTicket;
    @BindView(R.id.order_tickets_item_way)
    TextView mWay;
    @BindView(R.id.order_tickets_item_type)
    TextView mType;
    @BindView(R.id.order_tickets_item_dots)
    View mDots;

    private Context context;
    private OnQrListener listener;

    public TicketsAdapter(@NonNull Context context, int resource, @NonNull List<Ticket> objects, OnQrListener listener) {
        super(context, resource, objects);
        this.context = context;
        this.listener = listener;
    }

    @Override
    @NonNull
    @SuppressLint({"ViewHolder, InflateParams", "SetTextI18n"})
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_tickets, null);
        ButterKnife.bind(this, view);

        Ticket ticket = getItem(position);

        if (ticket != null) {
            mDots.setVisibility(position != getCount() - 1 ? View.VISIBLE : View.GONE);
            mWay.setText(ticket.getFrom() + " - " + ticket.getTo());
            mType.setText(ticket.getType());
            mTicket.setOnClickListener(v -> listener.onQr(ticket.getQrCode()));
        }

        return view;
    }

    public interface OnQrListener {
        void onQr(String qr);
    }
}
