package ru.appril.kraspg.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.appril.kraspg.R;
import ru.appril.kraspg.model.Schedule;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.StationViewHolder> {
    private List<Schedule> schedule;

    public ScheduleAdapter(List<Schedule> schedule) {
        this.schedule = schedule;
    }

    @Override
    @NonNull
    public StationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_schedule, parent, false);
        return new StationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StationViewHolder holder, int position) {
        Schedule schedule1 = schedule.get(position);

        holder.speed.setVisibility(schedule1.isSpeed() ? View.VISIBLE : View.GONE);

        if (!schedule1.isHasTransfers()) {
            holder.route_icon.setImageResource(schedule1.getThread().isExpress() ? R.mipmap.ic_express : R.mipmap.ic_train);
            holder.transfer_icon.setVisibility(View.GONE);

            holder.number.setVisibility(View.VISIBLE);
            holder.number.setText(schedule1.getThread().getNumber());
            holder.route.setText(schedule1.getThread().getRoute());

            holder.express.setVisibility(View.VISIBLE);
            holder.carrier.setVisibility(View.VISIBLE);
            holder.express.setVisibility(schedule1.getThread().isExpress() ? View.VISIBLE : View.GONE);
            holder.carrier.setText(schedule1.getThread().getCarrier());

            holder.from_subtitle.setText(schedule1.getDepartureSubtitle());
            holder.to_subtitle.setText(schedule1.getArrivalSubtitle());

            holder.from_time.setText(schedule1.getDepartureTime());
            holder.to_time.setText(schedule1.getArrivalTime());

            holder.from.setText(schedule1.getFrom());
            holder.to.setText(schedule1.getTo());

            holder.duration.setTextColor(schedule1.getThread().isExpress() ? 0xFFE11B1A : 0xFF9D9DA3);
            holder.duration.setText(schedule1.getDurationText());

            if (schedule1.getStops() != null && !schedule1.getStops().isEmpty()) {
                holder.stops_ll.setVisibility(View.VISIBLE);
                holder.stops.setText(schedule1.getStops());
            } else
                holder.stops_ll.setVisibility(View.GONE);

            if (schedule1.getDays() != null) {
                holder.days_ll.setVisibility(View.VISIBLE);
                holder.days.setText(schedule1.getDays());
            } else
                holder.days_ll.setVisibility(View.GONE);
        } else {
            holder.route_icon.setImageResource(schedule1.getDetails().get(0).getThread().isExpress() ?
                    R.mipmap.ic_express : R.mipmap.ic_train);
            holder.transfer_icon.setVisibility(View.VISIBLE);
            holder.transfer_icon.setImageResource(schedule1.getDetails().get(schedule1.getDetails().size() - 1)
                    .getThread().isExpress() ? R.mipmap.ic_express : R.mipmap.ic_train);

            holder.number.setVisibility(View.GONE);
            holder.route.setText("Пересадка в " + schedule1.getTransfer());

            holder.express.setVisibility(View.GONE);
            holder.carrier.setVisibility(View.GONE);

            holder.from_subtitle.setText(schedule1.getDepartureSubtitle());
            holder.to_subtitle.setText(schedule1.getArrivalSubtitle());

            holder.from_time.setText(schedule1.getDepartureTime());
            holder.to_time.setText(schedule1.getArrivalTime());

            holder.from.setText(schedule1.getFrom());
            holder.to.setText(schedule1.getTo());

            holder.duration.setTextColor(0xFF9D9DA3);
            holder.duration.setText(schedule1.getDurationText());

            holder.stops_ll.setVisibility(View.GONE);
            holder.days_ll.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return schedule.size();
    }

    static class StationViewHolder extends RecyclerView.ViewHolder {
        TextView speed, number, route, express, carrier, from_subtitle, to_subtitle,
                from_time, to_time, from, to, stops, days, duration;
        ImageView route_icon, transfer_icon;
        LinearLayout stops_ll, days_ll;

        StationViewHolder(View view) {
            super(view);

            speed = view.findViewById(R.id.schedule_item_speed);
            route_icon = view.findViewById(R.id.schedule_item_route_icon);
            transfer_icon = view.findViewById(R.id.schedule_item_transfer_icon);
            number = view.findViewById(R.id.schedule_item_number);
            route = view.findViewById(R.id.schedule_item_route);
            express = view.findViewById(R.id.schedule_item_express);
            carrier = view.findViewById(R.id.schedule_item_carrier);
            from_subtitle = view.findViewById(R.id.schedule_item_from_subtitle);
            to_subtitle = view.findViewById(R.id.schedule_item_to_subtitle);
            from_time = view.findViewById(R.id.schedule_item_from_time);
            to_time = view.findViewById(R.id.schedule_item_to_time);
            from = view.findViewById(R.id.schedule_item_from);
            to = view.findViewById(R.id.schedule_item_to);
            stops_ll = view.findViewById(R.id.schedule_item_stops_ll);
            stops = view.findViewById(R.id.schedule_item_stops);
            days_ll = view.findViewById(R.id.schedule_item_days_ll);
            days = view.findViewById(R.id.schedule_item_days);
            duration = view.findViewById(R.id.schedule_item_duration);
        }
    }
}
