package ru.appril.kraspg.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.appril.kraspg.R;
import ru.appril.kraspg.model.Station;

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.StationViewHolder>
        implements Filterable {

    private List<Station> original;
    private List<Station> stations;
    private OnItemClickListener listener;

    public StationAdapter(List<Station> stations, OnItemClickListener listener) {
        this.original = stations;
        this.stations = stations;
        this.listener = listener;
    }

    @Override
    @NonNull
    public StationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_station, parent, false);
        return new StationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StationViewHolder holder, int position) {
        Station station = stations.get(position);

        holder.station.setText(station.getStation());
        holder.bind(station, listener);
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filter = new FilterResults();
                List<Station> results = new ArrayList<>();

                if (constraint != null && constraint.length() > 0) {
                    if (original != null && original.size() > 0)
                        for (Station station : original)
                            if (station.getStation().toLowerCase().startsWith(constraint.toString().toLowerCase()))
                                results.add(station);
                } else
                    results = original;

                filter.values = results;
                filter.count = results.size();
                return filter;
            }

            @Override
            @SuppressWarnings("unchecked")
            protected void publishResults(CharSequence constraint, FilterResults results) {
                stations = (List<Station>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemClickListener {
        void onItemClick(Station station);
    }

    static class StationViewHolder extends RecyclerView.ViewHolder {
        TextView station;

        StationViewHolder(View view) {
            super(view);

            station = view.findViewById(R.id.station_item_station);
        }

        void bind(Station station, OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(station));
        }
    }
}
