package ru.appril.kraspg.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.appril.kraspg.R;
import ru.appril.kraspg.model.Passenger;

public class PassengersAdapter extends RecyclerView.Adapter<PassengersAdapter.StationViewHolder> {
    private List<Passenger> passengers;
    private boolean arrow;
    private OnItemClickListener listener;

    public PassengersAdapter(List<Passenger> passengers, boolean arrow, OnItemClickListener listener) {
        this.passengers = passengers;
        this.arrow = arrow;
        this.listener = listener;
    }

    @Override
    @NonNull
    public StationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_passengers, parent, false);
        return new StationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StationViewHolder holder, int position) {
        Passenger passenger = passengers.get(position);

        holder.full_name.setText(passenger.getFull_name());
        holder.document_type.setText(passenger.getDocument_type());
        holder.document_number.setText(passenger.getDocument_number());
        holder.arrow.setImageResource(arrow ? R.mipmap.ic_arrow_right_gray : R.mipmap.ic_delete_gray);

        holder.bind(passenger, position, listener);
    }

    @Override
    public int getItemCount() {
        return passengers.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Passenger passenger, int position);

        void onIconClick(Passenger passenger, int position);
    }

    static class StationViewHolder extends RecyclerView.ViewHolder {
        TextView full_name, document_type, document_number;
        ImageView arrow;

        StationViewHolder(View view) {
            super(view);

            full_name = view.findViewById(R.id.passengers_item_full_name);
            document_type = view.findViewById(R.id.passengers_item_document_type);
            document_number = view.findViewById(R.id.passengers_item_document_number);
            arrow = view.findViewById(R.id.passengers_item_arrow);
        }

        void bind(Passenger passenger, int position, OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(passenger, position));

            arrow.setOnClickListener(v -> listener.onIconClick(passenger, position));
        }
    }
}
