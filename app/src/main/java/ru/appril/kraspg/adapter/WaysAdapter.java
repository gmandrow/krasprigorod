package ru.appril.kraspg.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.model.Way;

public class WaysAdapter extends ArrayAdapter<Way> {
    @BindView(R.id.order_ways_item_from)
    TextView mFrom;
    @BindView(R.id.order_ways_item_to)
    TextView mTo;
    @BindView(R.id.order_ways_item_cost)
    TextView mCost;

    private Context context;

    public WaysAdapter(@NonNull Context context, int resource, @NonNull List<Way> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @Override
    @NonNull
    @SuppressLint("ViewHolder, InflateParams")
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_ways, null);
        ButterKnife.bind(this, view);

        Way way = getItem(position);

        if (way != null) {
            mFrom.setText(way.getFrom());
            mTo.setText(way.getTo());
            mCost.setText(way.getCost());
        }

        return view;
    }


}
