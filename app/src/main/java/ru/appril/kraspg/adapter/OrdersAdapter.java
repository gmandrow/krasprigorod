package ru.appril.kraspg.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.appril.kraspg.R;
import ru.appril.kraspg.model.Order;
import ru.appril.kraspg.model.Way;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.StationViewHolder> {
    private List<Order> orders;
    private OnItemClickListener listener;

    public OrdersAdapter(List<Order> orders, OnItemClickListener listener) {
        this.orders = orders;
        this.listener = listener;
    }

    @Override
    @NonNull
    public StationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        return new StationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StationViewHolder holder, int position) {
        Order order = orders.get(position);
        List<Way> ways = order.getWays();

        holder.date.setText(order.getDateFormat());
        holder.total.setText(order.getTotalFormat());
        holder.from.setText(ways.get(0).getFrom());
        holder.to.setText(ways.get(order.getWays().size() - 1).getTo());

        if (ways.size() > 1) {
            holder.transfer_ll.setVisibility(View.VISIBLE);
            holder.transfer.setText(ways.get(0).getTo());
        } else
            holder.transfer_ll.setVisibility(View.GONE);

        holder.bags.setEnabled(order.isBags());
        holder.animals.setEnabled(order.isAnimals());

        holder.not_archived.setVisibility(order.getArchived() == 0 ? View.VISIBLE : View.GONE);
        holder.archived.setVisibility(order.getArchived() == 1 ? View.VISIBLE : View.GONE);

        holder.bind(order, listener);
        holder.bind2(order, position, listener);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Order order);

        void onDeleteClick(Order order, int position);
    }

    static class StationViewHolder extends RecyclerView.ViewHolder {
        TextView date, total, from, transfer, to;
        FrameLayout not_archived, archived;
        LinearLayout transfer_ll;
        ImageView bags, animals;

        StationViewHolder(View view) {
            super(view);

            date = view.findViewById(R.id.order_item_date);
            total = view.findViewById(R.id.order_item_total);
            from = view.findViewById(R.id.order_item_from);
            transfer = view.findViewById(R.id.order_item_transfer);
            to = view.findViewById(R.id.order_item_to);
            transfer_ll = view.findViewById(R.id.order_item_transfer_ll);
            not_archived = view.findViewById(R.id.order_item_not_archived);
            archived = view.findViewById(R.id.order_item_archived);
            bags = view.findViewById(R.id.order_item_bags);
            animals = view.findViewById(R.id.order_item_animals);
        }

        void bind(Order order, OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(order));
        }

        void bind2(Order order, int position, OnItemClickListener listener) {
            archived.setOnClickListener(v -> listener.onDeleteClick(order, position));
        }
    }
}
