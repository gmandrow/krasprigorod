package ru.appril.kraspg;

import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.profile.Attribute;
import com.yandex.metrica.profile.UserProfile;

import ru.appril.kraspg.db.KpDatabaseFactory;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.utils.Const;
import ru.appril.kraspg.utils.Prefs;

public class Application extends android.app.Application {
    private static Application mInstance;

    private static void init(Application application) {
        mInstance = application;
    }

    public static Application getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Application.init(this);
        KpDatabaseFactory.initDatabase(this);
        RetrofitService.setToken(Prefs.init(this).getValue(Const.PREFS_TOKEN, Const.PREFS_DEFAULT_STRING));

        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder(Const.YANDEX).build();
        YandexMetrica.activate(this, config);
        YandexMetrica.enableActivityAutoTracking(this);
        setUserProfile();
    }

    public void setUserProfile() {
        Prefs mPref = Prefs.init(this);
        String user_id = String.valueOf(mPref.getValue(Const.PREFS_USER_ID, Const.PREFS_DEFAULT_INT));
        String token = mPref.getValue(Const.PREFS_TOKEN, Const.PREFS_DEFAULT_STRING);
        String email = mPref.getValue(Const.PREFS_EMAIL, Const.PREFS_DEFAULT_STRING);

        if (!token.isEmpty()) {
            UserProfile userProfile = UserProfile.newBuilder()
                    .apply(Attribute.customString(Const.PREFS_EMAIL).withValue(email)).build();
            YandexMetrica.setUserProfileID(user_id);
            YandexMetrica.reportUserProfile(userProfile);
        }
    }
}