package ru.appril.kraspg.db;

import android.content.Context;

import androidx.room.Room;

public class KpDatabaseFactory {
    private static KpDatabase mInstance;

    public static KpDatabase getInstance() {
        return mInstance;
    }

    public static void initDatabase(Context context) {
        mInstance = Room
                .databaseBuilder(context, KpDatabase.class, "krasprigorod.db")
                .fallbackToDestructiveMigration()
                .build();
    }
}