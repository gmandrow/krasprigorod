package ru.appril.kraspg.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import ru.appril.kraspg.dao.OrdersDao;
import ru.appril.kraspg.dao.PassengersDao;
import ru.appril.kraspg.dao.StationsDao;
import ru.appril.kraspg.model.Order;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.model.Station;

@Database(entities = {Station.class, Order.class, Passenger.class}, version = 1, exportSchema = false)
public abstract class KpDatabase extends RoomDatabase {
    public abstract StationsDao getStationsDao();

    public abstract OrdersDao getOrdersDao();

    public abstract PassengersDao getPassengersDao();

    public void deleteAll() {
        getOrdersDao().deleteOrders();
        getPassengersDao().deletePassengers();
    }
}