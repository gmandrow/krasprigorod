package ru.appril.kraspg.dao;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import ru.appril.kraspg.model.Passenger;

@Dao
public interface PassengersDao {
    @Query("SELECT * FROM passengers ORDER BY passenger_id DESC")
    List<Passenger> getPassengers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPassengers(List<Passenger> passengers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPassenger(Passenger passenger);

    @Query("DELETE FROM passengers")
    void deletePassengers();

    @Query("DELETE FROM passengers WHERE passenger_id = :passenger_id")
    void deletePassenger(int passenger_id);
}