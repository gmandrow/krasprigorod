package ru.appril.kraspg.dao;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import ru.appril.kraspg.model.Order;

@Dao
public interface OrdersDao {
    @Query("SELECT * FROM orders ORDER BY archived ASC, order_id DESC")
    List<Order> getOrders();

    @Query("SELECT * FROM orders WHERE order_id = :order_id")
    Order getOrder(int order_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrders(List<Order> orders);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrder(Order order);

    @Query("DELETE FROM orders")
    void deleteOrders();

    @Query("DELETE FROM orders WHERE order_id = :order_id")
    void deleteOrder(int order_id);
}