package ru.appril.kraspg.dao;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import ru.appril.kraspg.model.Station;

@Dao
public interface StationsDao {
    @Query("SELECT * FROM stations ORDER BY station")
    List<Station> getStations();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStations(List<Station> stations);
}