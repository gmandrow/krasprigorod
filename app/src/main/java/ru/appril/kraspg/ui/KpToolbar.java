package ru.appril.kraspg.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ru.appril.kraspg.R;

public class KpToolbar extends LinearLayout {
    @BindView(R.id.kp_toolbar_status_bar)
    View mStatusBar;
    @BindView(R.id.kp_toolbar_btn_navigation)
    ImageView mNavigation;
    @BindView(R.id.kp_toolbar_logo)
    ImageView mLogo;
    @BindView(R.id.kp_toolbar_btn_action)
    TextView mAction;
    @BindView(R.id.kp_toolbar_date)
    TextView mDate;
    @BindView(R.id.kp_toolbar_rl)
    RelativeLayout mRl;
    @BindView(R.id.kp_toolbar_title)
    TextView mTitle;
    @BindView(R.id.kp_toolbar_search)
    EditText mSearch;

    private OnNavigationListener mNavigationListener;
    private OnActionListener mActionListener;
    private OnSearchListener mSearchListener;

    private CharSequence text_action, text_title;
    private boolean enable_title, enable_date, enable_search, enable_action, enable_navigation,
            enable_status_bar, enable_logo;
    private int color_title, icon_navigation;

    public KpToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = getContext().obtainStyledAttributes(attrs, R.styleable.KpToolbar);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_kp_toolbar, this, true);
        ButterKnife.bind(this);

        try {
            initAttribute(attributes);
        } finally {
            attributes.recycle();
        }

        initViews();
    }

    private void initAttribute(TypedArray attributes) {
        text_action = attributes.getText(R.styleable.KpToolbar_kp_toolbarAction);
        text_title = attributes.getText(R.styleable.KpToolbar_kp_toolbarTitle);

        enable_title = attributes.getBoolean(R.styleable.KpToolbar_kp_toolbarTitleEnable, true);
        enable_date = attributes.getBoolean(R.styleable.KpToolbar_kp_toolbarDateEnable, false);
        enable_search = attributes.getBoolean(R.styleable.KpToolbar_kp_toolbarSearchEnable, false);
        enable_action = attributes.getBoolean(R.styleable.KpToolbar_kp_toolbarActionEnable, false);
        enable_navigation = attributes.getBoolean(R.styleable.KpToolbar_kp_toolbarNavigationEnable, false);
        enable_status_bar = attributes.getBoolean(R.styleable.KpToolbar_kp_toolbarStatusBarEnable, false);
        enable_logo = attributes.getBoolean(R.styleable.KpToolbar_kp_toolbarLogoEnable, true);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O_MR1)
            enable_status_bar = true;

        color_title = attributes.getColor(R.styleable.KpToolbar_kp_toolbarTitleColor, getResources().getColor(R.color.colorBlack));

        icon_navigation = attributes.getResourceId(R.styleable.KpToolbar_kp_toolbarNavigationIcon, R.mipmap.ic_back);
    }

    private void initViews() {
        mRl.setVisibility(enable_title || enable_search ? VISIBLE : GONE);
        mTitle.setVisibility(enable_title ? VISIBLE : GONE);
        mDate.setVisibility(enable_date ? VISIBLE : GONE);
        mSearch.setVisibility(enable_search ? VISIBLE : GONE);
        mAction.setVisibility(enable_action ? VISIBLE : GONE);
        mNavigation.setVisibility(enable_navigation ? VISIBLE : GONE);
        mStatusBar.setVisibility(enable_status_bar ? VISIBLE : GONE);
        mLogo.setVisibility(enable_logo ? VISIBLE : GONE);

        if (enable_action) mAction.setText(text_action);
        if (enable_date) mDate.setText(getCurrentDate());
        if (enable_title) {
            mTitle.setTextColor(color_title);
            mTitle.setText(text_title);
        }

        if (enable_navigation) mNavigation.setImageResource(icon_navigation);
    }

    @OnClick(R.id.kp_toolbar_btn_navigation)
    public void OnClickNavigation() {
        mNavigationListener.onClickNavigation();
    }

    @OnClick(R.id.kp_toolbar_btn_action)
    public void OnClickAction() {
        mActionListener.onClickAction();
    }

    @OnTextChanged(R.id.kp_toolbar_search)
    public void OnTextChangedSearch(CharSequence s) {
        if (mSearchListener != null)
            mSearchListener.onSearch(s.toString());
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }

    private String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("EEEE, d MMMM", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    public void setActionVisible(int visible) {
        mAction.setVisibility(visible);
    }

    public void setActionText(int text) {
        mAction.setText(text);
    }

    public void setOnNavigationListener(OnNavigationListener listener) {
        mNavigationListener = listener;
    }

    public void setOnActionListener(OnActionListener listener) {
        mActionListener = listener;
    }

    public void setOnSearchListener(OnSearchListener listener) {
        mSearchListener = listener;
    }

    public interface OnNavigationListener {
        void onClickNavigation();
    }

    public interface OnActionListener {
        void onClickAction();
    }

    public interface OnSearchListener {
        void onSearch(String search);
    }
}
