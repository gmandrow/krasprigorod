package ru.appril.kraspg.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.appril.kraspg.R;
import ru.appril.kraspg.fragment.DateFragment;

public class KpDate extends LinearLayout implements DateFragment.OnDateListener {
    @BindView(R.id.kp_date_today)
    TextView today;
    @BindView(R.id.kp_date_tomorrow)
    TextView tomorrow;
    @BindView(R.id.kp_date_date)
    TextView date;

    @BindColor(R.color.colorRed)
    int color_red;
    @BindColor(R.color.colorGray)
    int color_gray;

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private DateFormat dateFormat1 = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private String select_date;

    private OnDateListener mDateListener;

    public KpDate(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_kp_date, this, true);
        ButterKnife.bind(this);

        select_date = dateFormat.format(new Date());
    }

    @OnClick(R.id.kp_date_today)
    void onClickToday() {
        today.setTextColor(color_red);
        tomorrow.setTextColor(color_gray);
        date.setTextColor(color_gray);
        date.setText(R.string.search_date);

        Calendar now = Calendar.getInstance();
        select_date = dateFormat.format(now.getTime());
        mDateListener.onSelectedDate(select_date);
    }

    @OnClick(R.id.kp_date_tomorrow)
    void onClickTomorrow() {
        today.setTextColor(color_gray);
        tomorrow.setTextColor(color_red);
        date.setTextColor(color_gray);
        date.setText(R.string.search_date);

        Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, 1);
        select_date = dateFormat.format(now.getTime());
        mDateListener.onSelectedDate(select_date);
    }

    @OnClick(R.id.kp_date_date)
    void onClickDate() {
        Calendar now = Calendar.getInstance();
        Calendar now10 = Calendar.getInstance();
        now10.add(Calendar.DAY_OF_MONTH, 10);

        Bundle bundle = new Bundle();
        bundle.putLong("min_date", now.getTimeInMillis());
        bundle.putLong("max_date", now10.getTimeInMillis());

        DateFragment dateFragment = new DateFragment();
        dateFragment.setOnDateListener(this);
        dateFragment.setArguments(bundle);
        dateFragment.setCancelable(false);
        dateFragment.show(((AppCompatActivity) getContext()).getSupportFragmentManager(), DateFragment.TAG);
    }

    @Override
    public void onDate(SelectedDate selectedDate) {
        select_date = dateFormat.format(selectedDate.getFirstDate().getTime());

        today.setTextColor(color_gray);
        tomorrow.setTextColor(color_gray);
        date.setTextColor(color_red);
        date.setText(dateFormat1.format(selectedDate.getFirstDate().getTime()));
        mDateListener.onSelectedDate(select_date);
    }

    public String getDate() {
        return select_date;
    }

    public void setDate(String date) {
        select_date = date;
        mDateListener.onSelectedDate(select_date);
    }

    public void setOnDateListener(OnDateListener listener) {
        mDateListener = listener;
        mDateListener.onSelectedDate(select_date);
    }

    public interface OnDateListener {
        void onSelectedDate(String date);
    }
}
