package ru.appril.kraspg.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.appril.kraspg.R;

public class KpErrorView extends LinearLayout {
    private LinearLayout mError;
    private TextView mTitle;

    public KpErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_kp_error, this, true);

        mError = findViewById(R.id.error);
        mTitle = findViewById(R.id.error_title);
    }

    public KpErrorView setTitle(String title) {
        mTitle.setText(title);
        mTitle.setVisibility(VISIBLE);
        return this;
    }

    public void show() {
        mError.setVisibility(VISIBLE);
    }

    public void hide() {
        mTitle.setVisibility(GONE);
        mError.setVisibility(GONE);
    }
}
