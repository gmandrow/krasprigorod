package ru.appril.kraspg.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;

public class KpSelect extends LinearLayout {
    @BindView(R.id.kp_select_icon)
    ImageView icon;
    @BindView(R.id.kp_select_text)
    TextView text;

    private String hint;
    private int color, color_hint;

    public KpSelect(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.KpSelect);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_kp_select, this, true);
        ButterKnife.bind(this);

        try {
            icon.setImageResource(typedArray.getResourceId(R.styleable.KpSelect_kp_selectIcon, 0));

            color = typedArray.getColor(R.styleable.KpSelect_kp_selectColor, 0);
            color_hint = typedArray.getColor(R.styleable.KpSelect_kp_selectHintColor, 0);
            hint = typedArray.getText(R.styleable.KpSelect_kp_selectHint).toString();

            text.setTextColor(color_hint);
            text.setText(hint);
        } finally {
            typedArray.recycle();
        }
    }

    public String getText() {
        if (this.text.getText().toString().equals(hint))
            return "";
        else
            return this.text.getText().toString();
    }

    public void setText(String text) {
        this.text.setTextColor(text.isEmpty() ? color_hint : color);
        this.text.setText(text.isEmpty() ? hint : text);
    }
}