package ru.appril.kraspg.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.appril.kraspg.R;

public class KpCheck extends LinearLayout {
    @BindView(R.id.kp_check_icon)
    ImageView icon;
    @BindView(R.id.kp_check_text)
    TextView text;

    private OnCheckListener mCheckListener;

    private int icon_enable, icon_disable, color_disable, color_uncheck, color_check;
    private boolean check = false, disable = false;

    public KpCheck(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.KpCheck);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_kp_check, this, true);
        ButterKnife.bind(this);

        try {
            icon_enable = typedArray.getResourceId(R.styleable.KpCheck_kp_checkIcon, 0);
            icon_disable = typedArray.getResourceId(R.styleable.KpCheck_kp_checkDisableIcon, 0);
            icon.setImageResource(icon_enable);

            color_disable = typedArray.getColor(R.styleable.KpCheck_kp_checkDisableColor, 0);
            color_uncheck = typedArray.getColor(R.styleable.KpCheck_kp_checkUncheckColor, 0);
            color_check = typedArray.getColor(R.styleable.KpCheck_kp_checkCheckColor, 0);

            icon.setEnabled(false);
            text.setTextColor(color_uncheck);
            text.setText(typedArray.getText(R.styleable.KpCheck_kp_checkText));
        } finally {
            typedArray.recycle();
        }
    }

    @OnClick(R.id.kp_check_ll)
    void onClickCheck() {
        if (!disable)
            setCheck();
    }

    public void setCheck(boolean check) {
        this.check = check;
        icon.setEnabled(check);
        text.setTextColor(this.check ? color_check : color_uncheck);

        if (mCheckListener != null)
            mCheckListener.onCheck(text.getText().toString(), this.check ? 1 : 0);
    }

    private void setCheck() {
        setCheck(!check);
    }

    public void setDisable() {
        disable = true;
        icon.setImageResource(icon_disable);
        text.setTextColor(color_disable);
    }

    public void setEnable() {
        disable = false;
        icon.setImageResource(icon_enable);
        setCheck(check);
    }

    public void setOnCheckListener(OnCheckListener listener) {
        mCheckListener = listener;
    }

    public interface OnCheckListener {
        void onCheck(String text, int check);
    }
}