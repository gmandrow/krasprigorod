package ru.appril.kraspg.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;

public class KpButton extends LinearLayout {
    @BindView(R.id.button_text)
    TextView text;

    private int color_disable;

    public KpButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.KpButton);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_kp_btn, this, true);
        ButterKnife.bind(this);

        try {
            color_disable = typedArray.getColor(R.styleable.KpButton_kp_buttonTextColorDisable,
                    getResources().getColor(R.color.colorWhite));

            text.setText(typedArray.getText(R.styleable.KpButton_kp_buttonText));
            text.setBackgroundResource(typedArray.getResourceId(R.styleable.KpButton_kp_buttonBackground,
                    R.drawable.kp_btn_gray));
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        text.setEnabled(enabled);
        text.setTextColor(enabled ? getResources().getColor(R.color.colorWhite) : color_disable);
    }

    public void setText(String text) {
        this.text.setText(text);
    }
}