package ru.appril.kraspg.diff;

import java.util.List;

import androidx.recyclerview.widget.DiffUtil;
import ru.appril.kraspg.model.Order;

public class OrdersDiff extends DiffUtil.Callback {
    private final List<Order> oldList;
    private final List<Order> newList;

    public OrdersDiff(List<Order> oldList, List<Order> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Order oldProduct = oldList.get(oldItemPosition);
        Order newProduct = newList.get(newItemPosition);
        return oldProduct.getOrder_id() == newProduct.getOrder_id();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Order oldProduct = oldList.get(oldItemPosition);
        Order newProduct = newList.get(newItemPosition);
        return oldProduct.getArchived() == newProduct.getArchived();
    }
}