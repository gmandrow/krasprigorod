package ru.appril.kraspg.diff;

import java.util.List;

import androidx.recyclerview.widget.DiffUtil;
import ru.appril.kraspg.model.Passenger;

public class PassengersDiff extends DiffUtil.Callback {
    private final List<Passenger> oldList;
    private final List<Passenger> newList;

    public PassengersDiff(List<Passenger> oldList, List<Passenger> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Passenger oldProduct = oldList.get(oldItemPosition);
        Passenger newProduct = newList.get(newItemPosition);
        return oldProduct.getPassenger_id() == newProduct.getPassenger_id();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Passenger oldProduct = oldList.get(oldItemPosition);
        Passenger newProduct = newList.get(newItemPosition);
        return oldProduct.getDocument_number().equals(newProduct.getDocument_number())
                && oldProduct.getDocument_type().equals(newProduct.getDocument_type())
                && oldProduct.getFull_name().equals(newProduct.getFull_name());
    }
}