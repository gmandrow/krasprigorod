package ru.appril.kraspg.model;

import org.parceler.Parcel;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "passengers")
@Parcel(Parcel.Serialization.BEAN)
public class Passenger {
    @PrimaryKey
    private int passenger_id;

    private String full_name;

    private String document_type;

    private String document_number;

    public int getPassenger_id() {
        return passenger_id;
    }

    public void setPassenger_id(int passenger_id) {
        this.passenger_id = passenger_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }
}