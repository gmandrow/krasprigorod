package ru.appril.kraspg.model;

import java.util.Locale;

public class Way {
    private float cost;
    private String from;
    private String to;

    public String getCost() {
        return String.format(Locale.getDefault(), "%.2f \u20BD", cost);
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}
