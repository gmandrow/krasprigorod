package ru.appril.kraspg.model;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
public class Search {
    private Station from;
    private Station to;
    private String date;
    private int bags = 0;
    private int animals = 0;
    private int passenger_id = 0;

    public Station getFrom() {
        return from;
    }

    public void setFrom(Station from) {
        this.from = from;
    }

    public Station getTo() {
        return to;
    }

    public void setTo(Station to) {
        this.to = to;
    }

    public void swapStations() {
        Station from = getFrom();
        setFrom(getTo());
        setTo(from);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getBags() {
        return bags;
    }

    public void setBags(int bags) {
        this.bags = bags;
    }

    public int getAnimals() {
        return animals;
    }

    public void setAnimals(int animals) {
        this.animals = animals;
    }

    public int getPassengerId() {
        return passenger_id;
    }

    public void setPassengerId(int passenger_id) {
        this.passenger_id = passenger_id;
    }

    public boolean isFull() {
        return getFrom() != null && getTo() != null && getDate() != null;
    }
}