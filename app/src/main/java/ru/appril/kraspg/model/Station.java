package ru.appril.kraspg.model;

import org.parceler.Parcel;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "stations")
@Parcel(Parcel.Serialization.BEAN)
public class Station {
    @PrimaryKey
    private int station_id;

    private String station;

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
}