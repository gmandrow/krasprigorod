package ru.appril.kraspg.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import ru.appril.kraspg.converter.PassengerConverter;
import ru.appril.kraspg.converter.TicketsConverter;
import ru.appril.kraspg.converter.WaysConverter;

@Entity(tableName = "orders")
public class Order {
    @PrimaryKey
    private int order_id;

    private float total;

    private float bags;

    private float animals;

    private String date;

    private String route;

    private String status;

    private int archived;

    @TypeConverters({PassengerConverter.class})
    private Passenger passenger;

    @TypeConverters({WaysConverter.class})
    private List<Way> ways;

    @TypeConverters({TicketsConverter.class})
    private List<Ticket> tickets;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getTotalFormat() {
        return String.format(Locale.getDefault(), "%.2f \u20BD", total);
    }

    public float getBags() {
        return bags;
    }

    public String getBagsFormat() {
        return String.format(Locale.getDefault(), "%.2f \u20BD", bags);
    }

    public boolean isBags() {
        return bags > 0.0f;
    }

    public void setBags(float bags) {
        this.bags = bags;
    }

    public float getAnimals() {
        return animals;
    }

    public String getAnimalsFormat() {
        return String.format(Locale.getDefault(), "%.2f \u20BD", animals);
    }

    public boolean isAnimals() {
        return animals > 0.0f;
    }

    public void setAnimals(float animals) {
        this.animals = animals;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateFormat() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        DateFormat dateFormat1 = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

        try {
            return dateFormat1.format(dateFormat.parse(date));
        } catch (ParseException e) {
            return date;
        }
    }

    public String getDateFull() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        DateFormat dateFormat1 = new SimpleDateFormat("EEEE, d MMMM", Locale.getDefault());

        try {
            return dateFormat1.format(dateFormat.parse(date));
        } catch (ParseException e) {
            return date;
        }
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getArchived() {
        return archived;
    }

    public void setArchived(int archived) {
        this.archived = archived;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public List<Way> getWays() {
        return ways;
    }

    public void setWays(List<Way> ways) {
        this.ways = ways;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
