package ru.appril.kraspg.model;

public class Thread {
    private String number;
    private String route;
    private String carrier;
    private boolean express;

    public String getNumber() {
        return number;
    }

    public String getRoute() {
        return route;
    }

    public String getCarrier() {
        return carrier;
    }

    public boolean isExpress() {
        return express;
    }
}