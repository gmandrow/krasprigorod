package ru.appril.kraspg.model;

public class Ticket {
    private int ticket_id;
    private float cost;
    private String type;
    private String from;
    private String to;
    private String qr_code;

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getQrCode() {
        return qr_code;
    }
}
