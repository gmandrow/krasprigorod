package ru.appril.kraspg.model;

import androidx.annotation.NonNull;

public class Error {
    private int code;
    private String message;

    public Error(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    @NonNull
    public String toString() {
        return "code: " + getCode() + " message: " + getMessage();
    }
}
