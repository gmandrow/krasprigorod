package ru.appril.kraspg.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Schedule {
    private Thread thread;
    private String from;
    private String to;
    private String transfer;
    private String stops;
    private String days;
    private String departure;
    private String arrival;
    private int duration;
    private boolean has_transfers;
    private boolean speed;
    private List<Detail> details;

    public Thread getThread() {
        return thread;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getTransfer() {
        return transfer;
    }

    public String getStops() {
        return stops;
    }

    public String getDays() {
        return days;
    }

    private boolean isDate() {
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            return dateFormat2.format(dateFormat1.parse(getDeparture()))
                    .equals(dateFormat2.format(dateFormat1.parse(getArrival())));
        } catch (ParseException e) {
            return false;
        }
    }

    public String getDeparture() {
        return departure;
    }

    public String getDepartureSubtitle() {
        if (getDays() != null) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("МСК HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(new Date(dateFormat.parse(getDeparture()).getTime() - 14400000));
            } catch (ParseException e) {
                return getDeparture();
            }
        } else if (isDate()) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("МСК HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(new Date(dateFormat.parse(getDeparture()).getTime() - 14400000));
            } catch (ParseException e) {
                return getDeparture();
            }
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("d MMM | МСК HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(new Date(dateFormat.parse(getDeparture()).getTime() - 14400000));
            } catch (ParseException e) {
                return getDeparture();
            }
        }
    }

    public String getDepartureTime() {
        if (getDays() != null)
            return getDeparture();
        else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(dateFormat.parse(getDeparture()));
            } catch (ParseException e) {
                return getDeparture();
            }
        }
    }

    public String getArrival() {
        return arrival;
    }

    public String getArrivalSubtitle() {
        if (getDays() != null) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("МСК HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(new Date(dateFormat.parse(getArrival()).getTime() - 14400000));
            } catch (ParseException e) {
                return getArrival();
            }
        } else if (isDate()) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("МСК HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(new Date(dateFormat.parse(getArrival()).getTime() - 14400000));
            } catch (ParseException e) {
                return getArrival();
            }
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("d MMM | МСК HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(new Date(dateFormat.parse(getArrival()).getTime() - 14400000));
            } catch (ParseException e) {
                return getArrival();
            }
        }
    }

    public String getArrivalTime() {
        if (getDays() != null)
            return getArrival();
        else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            DateFormat dateFormat1 = new SimpleDateFormat("HH:mm", Locale.getDefault());

            try {
                return dateFormat1.format(dateFormat.parse(getArrival()));
            } catch (ParseException e) {
                return getArrival();
            }
        }
    }

    public int getDuration() {
        return duration;
    }

    public String getDurationText() {
        int hours = duration / 3600;
        int remainder = duration - hours * 3600;
        int minutes = remainder / 60;

        if (hours > 0)
            return String.format(Locale.getDefault(), "%d ч %d мин", hours, minutes);
        else
            return String.format(Locale.getDefault(), "%d мин", minutes);
    }

    public boolean isHasTransfers() {
        return has_transfers;
    }

    public boolean isSpeed() {
        return speed;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public class Detail {
        private Thread thread;
        private String from;
        private String to;
        private String transfer;
        private String stops;
        private String departure;
        private String arrival;
        private int duration;
        private boolean is_transfer;

        public Thread getThread() {
            return thread;
        }

        public String getFrom() {
            return from;
        }

        public String getTo() {
            return to;
        }

        public String getTransfer() {
            return transfer;
        }

        public String getStops() {
            return stops;
        }

        public String getDeparture() {
            return departure;
        }

        public String getArrival() {
            return arrival;
        }

        public int getDuration() {
            return duration;
        }

        public boolean isIsTransfer() {
            return is_transfer;
        }
    }
}