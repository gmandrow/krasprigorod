package ru.appril.kraspg.model;

import java.util.List;

public class Result {
    private User user;
    private Passenger passenger;
    private Order order;
    private Error error;

    private List<Station> stations;
    private List<Order> orders;
    private List<Passenger> passengers;
    private List<Schedule> schedule;

    public User getUser() {
        return user;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public Order getOrder() {
        return order;
    }

    public Error getError() {
        return error;
    }

    public List<Station> getStations() {
        return stations;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public List<Schedule> getSchedule() {
        return schedule;
    }
}
