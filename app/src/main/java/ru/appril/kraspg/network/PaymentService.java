package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import ru.appril.kraspg.model.Result;

public interface PaymentService {
    @FormUrlEncoded
    @POST("payment/order")
    Call<Result> getOrder(
            @Field("from") int from,
            @Field("to") int to,
            @Field("date") String date,
            @Field("bags") int bags,
            @Field("animals") int animals,
            @Field("passenger_id") int passenger_id);
}