package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.appril.kraspg.model.Result;

public interface ScheduleService {
    @GET("schedule/schedule")
    Call<Result> getSchedule(
            @Query("from") int from,
            @Query("to") int to,
            @Query("date") String date);
}