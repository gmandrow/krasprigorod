package ru.appril.kraspg.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ru.appril.kraspg.utils.Authentication;

public class RetrofitService {
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static String token;

    public static <S> S create(Class<S> api) {
        if (!token.isEmpty()) {
            Authentication interceptor = new Authentication(token);

            if (!httpClient.interceptors().contains(interceptor))
                httpClient.addInterceptor(interceptor);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl("https://www.kraspg.ru/api/v1/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(api);
    }

    public static void setToken(String token) {
        RetrofitService.token = token;
    }
}