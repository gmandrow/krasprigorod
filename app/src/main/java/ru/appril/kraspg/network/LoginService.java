package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.appril.kraspg.model.Result;

public interface LoginService {
    @FormUrlEncoded
    @POST("login/auth")
    Call<Result> Auth(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device") String device);

    @FormUrlEncoded
    @POST("login/reg")
    Call<Result> Reg(
            @Field("email") String email,
            @Field("password") String password,
            @Field("device") String device);

    @DELETE("user/password")
    Call<Result> deletePassword(
            @Query("email") String email);
}