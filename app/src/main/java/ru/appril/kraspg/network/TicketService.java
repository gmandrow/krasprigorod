package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.appril.kraspg.model.Result;

public interface TicketService {
    @GET("orders/payment")
    Call<String> isPaymentOrder(
            @Query("order_id") int order_id);

    @GET("orders/order")
    Call<Result> getTicket(
            @Query("order_id") int order_id);
}