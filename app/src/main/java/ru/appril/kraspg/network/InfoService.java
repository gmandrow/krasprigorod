package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface InfoService {
    @GET("info/{info}")
    Call<String> getInfo(@Path("info") String info);
}