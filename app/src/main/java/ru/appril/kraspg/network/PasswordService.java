package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.PUT;
import ru.appril.kraspg.model.Result;

public interface PasswordService {
    @FormUrlEncoded
    @PUT("user/password")
    Call<Result> changePassword(
            @Field("old_password") String old_password,
            @Field("new_password") String new_password);
}