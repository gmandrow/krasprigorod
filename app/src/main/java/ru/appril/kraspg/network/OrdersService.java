package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.appril.kraspg.model.Result;

public interface OrdersService {
    @GET("orders/orders")
    Call<Result> getOrders();

    @DELETE("orders/orders")
    Call<Result> deleteOrder(@Query("order_id") int order_id);
}