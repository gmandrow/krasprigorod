package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.appril.kraspg.model.Result;

public interface PassengerService {
    @GET("user/passengers")
    Call<Result> getPassengers();

    @FormUrlEncoded
    @POST("user/passengers")
    Call<Result> addPassenger(
            @Field("full_name") String full_name,
            @Field("document_type") String document_type,
            @Field("document_number") String document_number);

    @DELETE("user/passengers")
    Call<Void> deletePassenger(
            @Query("passenger_id") int passenger_id);
}