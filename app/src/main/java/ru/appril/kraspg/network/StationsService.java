package ru.appril.kraspg.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.appril.kraspg.model.Result;

public interface StationsService {
    @GET("stations/from")
    Call<Result> getStationsFrom();

    @GET("stations/to")
    Call<Result> getStationsTo(
            @Query("from") int from,
            @Query("date") String date);
}