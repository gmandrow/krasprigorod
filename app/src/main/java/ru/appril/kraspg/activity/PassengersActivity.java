package ru.appril.kraspg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.yandex.metrica.YandexMetrica;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.adapter.PassengersAdapter;
import ru.appril.kraspg.contact.PassengersContact;
import ru.appril.kraspg.diff.PassengersDiff;
import ru.appril.kraspg.fragment.PassengerFragment;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.model.Search;
import ru.appril.kraspg.presenter.PassengersPresenter;
import ru.appril.kraspg.ui.KpErrorView;
import ru.appril.kraspg.ui.KpToolbar;

public class PassengersActivity extends AppCompatActivity implements KpToolbar.OnNavigationListener,
        PassengersContact.View, KpToolbar.OnActionListener, PassengersAdapter.OnItemClickListener,
        PassengerFragment.OnPassengerListener {

    @BindView(R.id.passengers1_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.passengers1_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.passengers1_progress)
    ProgressBar mProgress;
    @BindView(R.id.passengers1_error)
    KpErrorView mError;

    private List<Passenger> passengers = new ArrayList<>();
    private Search search;

    private PassengersAdapter mAdapter;
    private PassengersPresenter mPassengersPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.passengers_title));
        setContentView(R.layout.activity_passengers);
        ButterKnife.bind(this);

        mToolbar.setOnNavigationListener(this);
        mToolbar.setOnActionListener(this);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setHasFixedSize(false);

        mAdapter = new PassengersAdapter(passengers, true, this);
        mRecycler.setAdapter(mAdapter);

        search = Parcels.unwrap(getIntent().getParcelableExtra(Search.class.getName()));
        mPassengersPresenter = new PassengersPresenter(this);
        mPassengersPresenter.getPassengersFromDb();
        new Handler().postDelayed(() -> mPassengersPresenter.getPassengers(), 1000);
    }

    @Override
    public void onClickNavigation() {
        super.onBackPressed();
    }

    @Override
    public void onClickAction() {
        showPassengerFragment();
    }

    @Override
    public void onItemClick(Passenger passenger, int position) {
        search.setPassengerId(passenger.getPassenger_id());
        startActivity(new Intent(this, PaymentActivity.class)
                .putExtra(Search.class.getName(), Parcels.wrap(search)));
    }

    @Override
    public void onIconClick(Passenger passenger, int position) {
    }

    @Override
    public void showPassengers(List<Passenger> passengers) {
        runOnUiThread(() -> {
            PassengersDiff passengersDiff = new PassengersDiff(this.passengers, passengers);
            DiffUtil.DiffResult passengersDiffResult = DiffUtil.calculateDiff(passengersDiff);

            this.passengers.clear();
            this.passengers.addAll(passengers);
            passengersDiffResult.dispatchUpdatesTo(mAdapter);
            showError(new Error(0, "Добавьте пассажира"));
        });
    }

    @Override
    public void showError(Error error) {
        runOnUiThread(() -> {
            if (passengers.size() == 0)
                mError.setTitle(error.getMessage()).show();
            else
                mError.hide();
        });
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> {
            if (passengers.size() == 0)
                mProgress.setVisibility(View.VISIBLE);
        });

    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> mProgress.setVisibility(View.GONE));
    }

    @Override
    public void onPassenger(Passenger passenger) {
        this.passengers.add(0, passenger);
        mAdapter.notifyItemInserted(0);
        showError(new Error(0, "Добавьте пассажира"));
    }

    private void showPassengerFragment() {
        PassengerFragment passengerFragment = new PassengerFragment();
        passengerFragment.setOnPassengerListener(this);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        passengerFragment.show(fragmentTransaction, PassengerFragment.TAG);
    }
}