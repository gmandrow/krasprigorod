package ru.appril.kraspg.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.yandex.metrica.YandexMetrica;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.adapter.ScheduleAdapter;
import ru.appril.kraspg.contact.ScheduleContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Schedule;
import ru.appril.kraspg.model.Search;
import ru.appril.kraspg.presenter.SchedulePresenter;
import ru.appril.kraspg.ui.KpErrorView;
import ru.appril.kraspg.ui.KpToolbar;

public class ScheduleActivity extends AppCompatActivity implements KpToolbar.OnNavigationListener,
        ScheduleContact.View {

    @BindView(R.id.schedule_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.schedule_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.schedule_progress)
    ProgressBar mProgress;
    @BindView(R.id.schedule_error)
    KpErrorView mError;

    private List<Schedule> schedule = new ArrayList<>();

    private ScheduleAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.schedule_title));
        setContentView(R.layout.activity_schedule);
        ButterKnife.bind(this);

        mToolbar.setOnNavigationListener(this);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setHasFixedSize(false);

        mAdapter = new ScheduleAdapter(schedule);
        mRecycler.setAdapter(mAdapter);

        Search search = Parcels.unwrap(getIntent().getParcelableExtra(Search.class.getName()));
        SchedulePresenter mSchedulePresenter = new SchedulePresenter(this);
        mSchedulePresenter.getSchedule(search.getFrom().getStation_id(), search.getTo().getStation_id(), search.getDate());
    }

    @Override
    public void onClickNavigation() {
        finish();
    }

    @Override
    public void showSchedule(List<Schedule> schedule) {
        runOnUiThread(() -> {
            this.schedule.clear();
            this.schedule.addAll(schedule);

            mAdapter.notifyDataSetChanged();

            if (schedule.size() == 0)
                mError.setTitle("Поездов не найдено").show();
            else
                mError.hide();
        });
    }

    @Override
    public void showError(Error error) {
        runOnUiThread(() -> mError.setTitle(error.getMessage()).show());
    }

    @Override
    public void showProgress() {
        runOnUiThread(() -> mProgress.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> mProgress.setVisibility(View.GONE));
    }
}