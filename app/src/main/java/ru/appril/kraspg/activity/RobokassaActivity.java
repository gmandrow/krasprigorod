package ru.appril.kraspg.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.yandex.metrica.YandexMetrica;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.contact.RobokassaContact;
import ru.appril.kraspg.presenter.RobokassaPresenter;
import ru.appril.kraspg.ui.KpToolbar;

public class RobokassaActivity extends AppCompatActivity implements KpToolbar.OnNavigationListener,
        RobokassaContact.View {

    @BindView(R.id.robokassa_status_bar)
    View mStatsBar;
    @BindView(R.id.robokassa_web)
    WebView mWeb;

    private int order_id;

    private RobokassaPresenter mRobokassaPresenter;

    @Override
    @SuppressLint("SetJavaScriptEnabled")
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Robokassa);
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent("Робокасса");
        setContentView(R.layout.activity_robokassa);
        ButterKnife.bind(this);

        order_id = getIntent().getIntExtra("order_id", 0);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O_MR1)
            mStatsBar.setVisibility(View.VISIBLE);

        mWeb.setWebChromeClient(new WebChromeClient());
        mWeb.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d("mlg", url);
                view.loadUrl(url);
                return true;
            }
        });

        WebSettings mWebSettings = mWeb.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setUseWideViewPort(true);

        mWeb.loadUrl("https://www.kraspg.ru/api/v1/payment/robokassa?order_id=" + order_id);

        mRobokassaPresenter = new RobokassaPresenter(this);
        mRobokassaPresenter.isPaymentOrder(order_id);
    }

    @Override
    public void onClickNavigation() {
        super.onBackPressed();
    }

    @Override
    public void isPaymentOrder(String payment) {
        if (payment.equals("Оплачен")) {
            startActivity(new Intent(RobokassaActivity.this, OrderActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .putExtra("order_id", order_id));
            finish();
        } else
            new Handler().postDelayed(() -> mRobokassaPresenter.isPaymentOrder(order_id), 1000);
    }
}
