package ru.appril.kraspg.activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.contact.TextContact;
import ru.appril.kraspg.presenter.TextPresenter;
import ru.appril.kraspg.ui.KpToolbar;

public class TextActivity extends AppCompatActivity implements KpToolbar.OnNavigationListener, TextContact.View {
    @BindView(R.id.text_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.text_text)
    TextView mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);
        ButterKnife.bind(this);
        mToolbar.setOnNavigationListener(this);

        String state = getIntent().getStringExtra("state");

        TextPresenter mTextPresenter = new TextPresenter(this);
        mTextPresenter.getInfo(state);
    }

    @Override
    public void onClickNavigation() {
        super.onBackPressed();
    }

    @Override
    public void showText(String text) {
        runOnUiThread(() -> mText.setText(text));
    }
}