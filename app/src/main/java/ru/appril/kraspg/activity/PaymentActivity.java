package ru.appril.kraspg.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.viewanimator.ViewAnimator;
import com.yandex.metrica.YandexMetrica;

import org.parceler.Parcels;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.appril.kraspg.R;
import ru.appril.kraspg.contact.PaymentContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Order;
import ru.appril.kraspg.model.Search;
import ru.appril.kraspg.presenter.PaymentPresenter;
import ru.appril.kraspg.ui.KpButton;
import ru.appril.kraspg.ui.KpToolbar;

public class PaymentActivity extends AppCompatActivity implements KpToolbar.OnNavigationListener,
        PaymentContact.View {

    @BindView(R.id.payment_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.payment_progress)
    ProgressBar mProgress;
    @BindView(R.id.payment_ll0)
    LinearLayout mLL0;
    @BindView(R.id.payment_date)
    TextView mDate;
    @BindView(R.id.payment_from)
    TextView mFrom;
    @BindView(R.id.payment_to)
    TextView mTo;
    @BindView(R.id.payment_cost)
    TextView mCost;
    @BindView(R.id.payment_ways2)
    LinearLayout mWays2;
    @BindView(R.id.payment_from2)
    TextView mFrom2;
    @BindView(R.id.payment_to2)
    TextView mTo2;
    @BindView(R.id.payment_cost2)
    TextView mCost2;
    @BindView(R.id.payment_bags)
    TextView mBags;
    @BindView(R.id.payment_animals)
    TextView mAnimals;
    @BindView(R.id.payment_result)
    TextView mResult;
    @BindView(R.id.payment_btn_pay)
    KpButton mBtnPay;

    private int order_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.payment_title));
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        Search search = Parcels.unwrap(getIntent().getParcelableExtra(Search.class.getName()));

        mToolbar.setOnNavigationListener(this);

        PaymentPresenter mPaymentPresenter = new PaymentPresenter(this);
        mPaymentPresenter.getOrder(search.getFrom().getStation_id(), search.getTo().getStation_id(),
                search.getDate(), search.getBags(), search.getAnimals(), search.getPassengerId());
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickNavigation() {
        finish();
    }

    @OnClick(R.id.payment_btn_pay)
    void onClickPay() {
        if (order_id != 0)
            startActivity(new Intent(this, RobokassaActivity.class)
                    .putExtra("order_id", order_id));
    }

    @Override
    @SuppressLint("SetTextI18n")
    public void showOrder(Order order) {
        runOnUiThread(() -> {
            order_id = order.getOrder_id();

            mDate.setText(order.getDateFull());

            mFrom.setText(order.getWays().get(0).getFrom());
            mTo.setText(order.getWays().get(0).getTo());
            mCost.setText(order.getWays().get(0).getCost());

            if (order.getWays().size() > 1) {
                mWays2.setVisibility(View.VISIBLE);
                mFrom2.setText(order.getWays().get(1).getFrom());
                mTo2.setText(order.getWays().get(1).getTo());
                mCost2.setText(order.getWays().get(1).getCost());
            }

            mBags.setText(order.getBagsFormat());
            mAnimals.setText(order.getAnimalsFormat());

            mResult.setText(order.getTotalFormat());
            mProgress.setVisibility(View.GONE);
            mLL0.setVisibility(View.VISIBLE);

            ViewAnimator
                    .animate(mLL0)
                    .duration(1000)
                    .slideTopIn()
                    .onStop(this::showBtn)
                    .start();
        });
    }

    private void showBtn() {
        runOnUiThread(() -> {
            mBtnPay.setVisibility(View.VISIBLE);

            ViewAnimator
                    .animate(mBtnPay)
                    .duration(500)
                    .slideBottomIn()
                    .start();
        });
    }

    @Override
    public void showError(Error error) {
        runOnUiThread(() -> {
            mProgress.setVisibility(View.GONE);
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
        });
    }
}