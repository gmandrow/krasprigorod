package ru.appril.kraspg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.florent37.viewanimator.ViewAnimator;
import com.yandex.metrica.YandexMetrica;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.adapter.TicketsAdapter;
import ru.appril.kraspg.adapter.WaysAdapter;
import ru.appril.kraspg.contact.OrderContact;
import ru.appril.kraspg.model.Order;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.presenter.TicketPresenter;
import ru.appril.kraspg.ui.KpItemsView;
import ru.appril.kraspg.ui.KpToolbar;

public class OrderActivity extends AppCompatActivity implements KpToolbar.OnNavigationListener,
        OrderContact.View, TicketsAdapter.OnQrListener {

    @BindView(R.id.ticket_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.order_progress)
    ProgressBar mProgress;
    @BindView(R.id.order_body)
    LinearLayout mOrder;
    @BindView(R.id.order_date)
    TextView mDate;
    @BindView(R.id.order_passenger_full_name)
    TextView mPassengerFullName;
    @BindView(R.id.ticket_passenger_document_type)
    TextView mPassengerDocumentType;
    @BindView(R.id.ticket_passenger_document_number)
    TextView mPassengerDocumentNumber;
    @BindView(R.id.order_ways)
    KpItemsView mWays;
    @BindView(R.id.order_bags)
    TextView mBags;
    @BindView(R.id.order_animals)
    TextView mAnimals;
    @BindView(R.id.order_total)
    TextView mTotal;
    @BindView(R.id.order_tickets)
    KpItemsView mTickets;

    private int order_id;
    private boolean finish;

    private TicketPresenter mTicketPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.order_title));
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);

        finish = getIntent().getBooleanExtra("finish", false);
        order_id = getIntent().getIntExtra("order_id", 0);

        mToolbar.setOnNavigationListener(this);
        mProgress.setVisibility(View.VISIBLE);

        mTicketPresenter = new TicketPresenter(this);
        mTicketPresenter.getOrderFromDb(order_id);
        new Handler().postDelayed(() -> mTicketPresenter.getTicket(order_id), 1000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onClickNavigation();
    }

    @Override
    public void onClickNavigation() {
        if (!finish)
            startActivity(new Intent(this, MainActivity.class)
                    .putExtra("navigation", R.id.navigation_tickets));
        finish();
    }

    @Override
    public void showTicket(Order order) {
        runOnUiThread(() -> {
            mDate.setText(order.getDateFull());

            Passenger passenger = order.getPassenger();
            mPassengerFullName.setText(passenger.getFull_name());
            mPassengerDocumentType.setText(passenger.getDocument_type());
            mPassengerDocumentNumber.setText(passenger.getDocument_number());

            mWays.setAdapter(new WaysAdapter(this, R.layout.item_order_ways, order.getWays()));

            mBags.setText(order.getBagsFormat());
            mAnimals.setText(order.getAnimalsFormat());
            mTotal.setText(order.getTotalFormat());

            if (order.getTickets() != null)
                mTickets.setAdapter(new TicketsAdapter(this, R.layout.item_order_tickets,
                        order.getTickets(), this));

            if (mOrder.getVisibility() == View.INVISIBLE)
                mOrder.post(() -> {
                    mProgress.setVisibility(View.GONE);
                    mOrder.setVisibility(View.VISIBLE);

                    ViewAnimator
                            .animate(mOrder)
                            .duration(1000)
                            .slideTopIn()
                            .start();
                });
            });
    }

    @Override
    public void onQr(String qr) {
        if (qr != null && !qr.isEmpty())
            startActivity(new Intent(this, QrActivity.class)
                    .putExtra("qr", qr));
    }
}