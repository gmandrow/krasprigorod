package ru.appril.kraspg.activity;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.yandex.metrica.YandexMetrica;

import net.glxn.qrgen.android.QRCode;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.ui.KpToolbar;
import ru.appril.kraspg.utils.Prefs;

public class QrActivity extends AppCompatActivity implements KpToolbar.OnNavigationListener {

    @BindView(R.id.qr_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.qr_qr)
    ImageView mQr;

    private float last_brightness;

    private Prefs mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.qr_title));
        setContentView(R.layout.activity_qr);
        ButterKnife.bind(this);
        mPref = Prefs.init(this);
        String qr = getIntent().getStringExtra("qr");

        mToolbar.setOnNavigationListener(this);
        mQr.post(() -> mQr.setImageBitmap(QRCode.from(qr).withSize(mQr.getWidth(), mQr.getHeight())
                .withColor(getResources().getColor(R.color.colorBlack),
                        getResources().getColor(R.color.colorGrayLight)).bitmap()));
        getBrightness();
    }

    @Override
    public void onResume() {
        super.onResume();
        last_brightness = mPref.getValue("last_brightness", 0.0f);
        setBrightness(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        setBrightness(false);
    }

    @Override
    public void onClickNavigation() {
        finish();
    }

    private void getBrightness() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        last_brightness = lp.screenBrightness;
        mPref.setValue("last_brightness", last_brightness);
    }

    private void setBrightness(boolean max) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = max ? WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL : last_brightness;
        getWindow().setAttributes(lp);
    }
}