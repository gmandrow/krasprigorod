package ru.appril.kraspg.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.appril.kraspg.R;
import ru.appril.kraspg.activity.TextActivity;

public class InfoFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.info_btn_feedback)
    void onClickFeedback() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:info@kraspg.ru?subject=Мобильное приложение"));
        startActivity(intent);
    }

    @OnClick(R.id.info_btn_rate)
    void onClickRate() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=ru.appril.kraspg"));
        startActivity(intent);
    }

    @OnClick(R.id.info_btn_confidential)
    void onClickConfidential() {
        startActivity(new Intent(getContext(), TextActivity.class)
                .putExtra("state", "confidential"));
    }

    @OnClick(R.id.info_btn_rules)
    void onClickRules() {
        startActivity(new Intent(getContext(), TextActivity.class)
                .putExtra("state", "rules"));
    }

    @OnClick(R.id.info_btn_regulations)
    void onClickRegulations() {
        startActivity(new Intent(getContext(), TextActivity.class)
                .putExtra("state", "regulations"));
    }
}