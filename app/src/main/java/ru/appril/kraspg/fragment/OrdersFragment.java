package ru.appril.kraspg.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.yandex.metrica.YandexMetrica;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.Application;
import ru.appril.kraspg.R;
import ru.appril.kraspg.activity.OrderActivity;
import ru.appril.kraspg.adapter.OrdersAdapter;
import ru.appril.kraspg.contact.OrdersContact;
import ru.appril.kraspg.diff.OrdersDiff;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Order;
import ru.appril.kraspg.model.User;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.presenter.OrdersPresenter;
import ru.appril.kraspg.ui.KpErrorView;
import ru.appril.kraspg.utils.Const;
import ru.appril.kraspg.utils.Prefs;

public class OrdersFragment extends Fragment implements OrdersAdapter.OnItemClickListener,
        OrdersContact.View, LoginFragment.OnLoginListener {

    @BindView(R.id.orders_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.orders_progress)
    ProgressBar mProgress;
    @BindView(R.id.orders_error)
    KpErrorView mError;

    private List<Order> orders = new ArrayList<>();

    private Prefs mPref;
    private OrdersAdapter mAdapter;
    private OrdersPresenter mOrdersPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.orders_title));
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setHasFixedSize(false);

        mAdapter = new OrdersAdapter(orders, this);
        mRecycler.setAdapter(mAdapter);

        mOrdersPresenter = new OrdersPresenter(this);
        mPref = Prefs.init(getContext());

        if (mPref.getValue(Const.PREFS_TOKEN, Const.PREFS_DEFAULT_STRING).isEmpty()) {
            mError.setTitle("Необходимо\nавторизоваться").show();
            showAuthFragment();
        } else {
            mOrdersPresenter.getOrdersFromDb();
            new Handler().postDelayed(() -> mOrdersPresenter.getOrders(), 1000);
        }
        return view;
    }

    @Override
    public void onItemClick(Order order) {
        startActivity(new Intent(getContext(), OrderActivity.class)
                .putExtra("finish", true)
                .putExtra("order_id", order.getOrder_id()));
    }

    @Override
    public void onDeleteClick(Order order, int position) {
        orders.remove(position);
        mAdapter.notifyItemRemoved(position);
        mAdapter.notifyItemRangeChanged(position, orders.size() - position);

        mOrdersPresenter.deleteOrderFromDb(order.getOrder_id());
        mOrdersPresenter.deleteOrder(order.getOrder_id());
    }

    @Override
    public void showOrders(List<Order> orders) {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                OrdersDiff ordersDiff = new OrdersDiff(this.orders, orders);
                DiffUtil.DiffResult ordersDiffResult = DiffUtil.calculateDiff(ordersDiff);

                this.orders.clear();
                this.orders.addAll(orders);

                ordersDiffResult.dispatchUpdatesTo(mAdapter);
                showError(new Error(0, "У вас нет заказов"));
            });
    }

    @Override
    public void showError(Error error) {
        if (getActivity() != null && orders.size() == 0)
            getActivity().runOnUiThread(() -> mError.setTitle(error.getMessage()).show());
    }

    @Override
    public void showProgress() {
        if (getActivity() != null && orders.size() == 0)
            getActivity().runOnUiThread(() -> mProgress.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> mProgress.setVisibility(View.GONE));

    }

    @Override
    public void onLogin(User user) {
        mPref.setValue(Const.PREFS_USER_ID, user.getUserId());
        mPref.setValue(Const.PREFS_TOKEN, user.getToken());
        mPref.setValue(Const.PREFS_EMAIL, user.getEmail());
        Application.getInstance().setUserProfile();
        RetrofitService.setToken(user.getToken());

        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                mError.hide();
                mOrdersPresenter = new OrdersPresenter(this);
                mOrdersPresenter.getOrders();
            });
    }

    private void showAuthFragment() {
        LoginFragment authFragment = new LoginFragment();
        authFragment.setOnLoginListener(this);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        authFragment.show(fragmentTransaction, LoginFragment.TAG);
    }
}