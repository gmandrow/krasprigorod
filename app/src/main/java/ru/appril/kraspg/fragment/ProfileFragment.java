package ru.appril.kraspg.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.yandex.metrica.YandexMetrica;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.Application;
import ru.appril.kraspg.R;
import ru.appril.kraspg.activity.MainActivity;
import ru.appril.kraspg.db.KpDatabaseFactory;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.model.User;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.ui.KpErrorView;
import ru.appril.kraspg.ui.KpToolbar;
import ru.appril.kraspg.utils.Const;
import ru.appril.kraspg.utils.Prefs;

public class ProfileFragment extends Fragment implements LoginFragment.OnLoginListener, KpToolbar.OnActionListener, ViewPager.OnPageChangeListener, PassengerFragment.OnPassengerListener {
    @BindView(R.id.profile_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.profile_tab)
    TabLayout mTab;
    @BindView(R.id.profile_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.profile_ll)
    LinearLayout mLL;
    @BindView(R.id.profile_error)
    KpErrorView mError;

    private String token;
    private int position;

    private Prefs mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.profile_title));
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        mToolbar.setOnActionListener(this);

        mPref = Prefs.init(getContext());
        token = mPref.getValue(Const.PREFS_TOKEN, Const.PREFS_DEFAULT_STRING);

        mViewPager.setAdapter(new TabAdapter(getChildFragmentManager()));
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setOffscreenPageLimit(1);

        mTab.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(1);

        if (token.isEmpty()) {
            mToolbar.setActionVisible(View.GONE);
            mLL.setVisibility(View.GONE);
            mError.setTitle("Необходимо\nавторизоваться").show();
            showAuthFragment();
        } else
            mToolbar.setActionVisible(View.VISIBLE);
        return view;
    }

    @Override
    public void onClickAction() {
        if (getContext() != null)
            if (position == 0)
                showPassengerFragment();
            else
                new AlertDialog.Builder(getContext())
                        .setTitle("Выход")
                        .setMessage("Вы действительно хотите выйти из своей учетной записи?")
                        .setNegativeButton(getString(android.R.string.cancel),
                                (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Выйти",
                                (dialog, which) -> {
                                    mPref.clear();
                                    AsyncTask.execute(() -> {
                                        KpDatabaseFactory.getInstance().deleteAll();
                                        startActivity(new Intent(getContext(), MainActivity.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                                .putExtra("navigation", R.id.navigation_search));
                                    });
                                })
                        .create()
                        .show();
    }

    @Override
    public void onLogin(User user) {
        token = user.getToken();
        mPref.setValue(Const.PREFS_USER_ID, user.getUserId());
        mPref.setValue(Const.PREFS_TOKEN, user.getToken());
        mPref.setValue(Const.PREFS_EMAIL, user.getEmail());
        Application.getInstance().setUserProfile();
        RetrofitService.setToken(user.getToken());

        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                mError.hide();
                mLL.setVisibility(View.VISIBLE);
                mToolbar.setActionVisible(View.VISIBLE);

                if (mViewPager.getAdapter() != null)
                    mViewPager.getAdapter().notifyDataSetChanged();
            });
    }

    @Override
    public void onPassenger(Passenger passenger) {
        if (mViewPager.getAdapter() != null)
            mViewPager.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onPageSelected(int position) {
        this.position = position;

        if (token.isEmpty() || position == 2)
            mToolbar.setActionVisible(View.GONE);
        else {
            mToolbar.setActionVisible(View.VISIBLE);
            mToolbar.setActionText(position == 0 ?
                    R.string.profile_action_add : R.string.profile_action_logout);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    private void showAuthFragment() {
        LoginFragment authFragment = new LoginFragment();
        authFragment.setOnLoginListener(this);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        authFragment.show(fragmentTransaction, LoginFragment.TAG);
    }

    private void showPassengerFragment() {
        PassengerFragment passengerFragment = new PassengerFragment();
        passengerFragment.setOnPassengerListener(this);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        passengerFragment.show(fragmentTransaction, PassengerFragment.TAG);
    }

    private class TabAdapter extends FragmentPagerAdapter {
        TabAdapter(FragmentManager mgr) {
            super(mgr);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Пассажиры";
                case 1:
                    return "Данные";
                case 2:
                    return "Информация";
                default:
                    return "";
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new PassengersFragment();
                case 1:
                    return new PersonFragment();
                case 2:
                    return new InfoFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getItemPosition(@NotNull Object object) {
            return POSITION_NONE;
        }
    }
}