package ru.appril.kraspg.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appeaser.sublimepickerlibrary.SublimePicker;
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.yandex.metrica.YandexMetrica;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class DateFragment extends DialogFragment {
    public static final String TAG = "DateFragment";

    private OnDateListener mDateListener;
    private long min_date, max_date;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent("Календарь");

        Bundle bundle = getArguments();

        if (bundle != null) {
            min_date = bundle.getLong("min_date");
            max_date = bundle.getLong("max_date");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        SublimePicker mSublimePicker = null;
        SublimeOptions mSublimeOptions = new SublimeOptions();
        SublimeListenerAdapter mSublimeListener = new SublimeListenerAdapter() {
            @Override
            public void onDateTimeRecurrenceSet(SublimePicker sublimeMaterialPicker,
                                                SelectedDate selectedDate, int hourOfDay, int minute,
                                                SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                                String recurrenceRule) {
                if (mDateListener != null)
                    mDateListener.onDate(selectedDate);

                dismiss();
            }

            @Override
            public void onCancelled() {
                dismiss();
            }
        };

        if (getContext() != null) {
            mSublimePicker = new SublimePicker(getContext());
            mSublimeOptions.setAnimateLayoutChanges(true);
            mSublimeOptions.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
            mSublimeOptions.setDateRange(min_date, max_date);
            mSublimePicker.initializePicker(mSublimeOptions, mSublimeListener);
        }

        return mSublimePicker;
    }

    public void setOnDateListener(OnDateListener listener) {
        mDateListener = listener;
    }

    public interface OnDateListener {
        void onDate(SelectedDate selectedDate);
    }
}