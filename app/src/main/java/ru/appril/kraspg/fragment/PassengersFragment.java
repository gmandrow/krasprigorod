package ru.appril.kraspg.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.adapter.PassengersAdapter;
import ru.appril.kraspg.contact.PassengersContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.presenter.PassengersPresenter;

public class PassengersFragment extends Fragment implements PassengersContact.View,
        PassengersAdapter.OnItemClickListener {

    @BindView(R.id.passengers_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.passengers_progress)
    ProgressBar mProgress;

    private List<Passenger> passengers = new ArrayList<>();

    private PassengersAdapter mAdapter;
    private PassengersPresenter mPassengersPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_passengers, container, false);
        ButterKnife.bind(this, view);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setHasFixedSize(false);

        mAdapter = new PassengersAdapter(passengers, false, this);
        mRecycler.setAdapter(mAdapter);

        mPassengersPresenter = new PassengersPresenter(this);
        mPassengersPresenter.getPassengersFromDb();
        new Handler().postDelayed(() -> mPassengersPresenter.getPassengers(), 1000);
        return view;
    }

    @Override
    public void showPassengers(List<Passenger> passengers) {
        this.passengers.clear();
        this.passengers.addAll(passengers);

        if (getActivity() != null)
            getActivity().runOnUiThread(() -> mAdapter.notifyDataSetChanged());
    }

    @Override
    public void showError(Error error) {
    }

    @Override
    public void showProgress() {
        if (getActivity() != null && passengers.size() == 0)
            getActivity().runOnUiThread(() -> mProgress.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> mProgress.setVisibility(View.GONE));
    }

    @Override
    public void onItemClick(Passenger passenger, int position) {
    }

    @Override
    public void onIconClick(Passenger passenger, int position) {
        if (getContext() != null)
            new AlertDialog.Builder(getContext())
                    .setTitle("Удаление пассажира")
                    .setMessage("Вы действительно хотите удалить пассажира: " + passenger.getFull_name() + "?")
                    .setNegativeButton(getString(android.R.string.cancel),
                            (dialog, which) -> dialog.dismiss())
                    .setPositiveButton("Удалить",
                            (dialog, which) -> {
                                passengers.remove(position);
                                mAdapter.notifyItemRemoved(position);
                                mAdapter.notifyItemRangeChanged(position, passengers.size() - position);
                                mPassengersPresenter.deletePassenger(passenger.getPassenger_id());
                            })
                    .create()
                    .show();
    }
}