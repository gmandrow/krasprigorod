package ru.appril.kraspg.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.florent37.viewanimator.ViewAnimator;
import com.google.gson.GsonBuilder;
import com.yandex.metrica.YandexMetrica;

import org.parceler.Parcels;

import java.util.Random;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.appril.kraspg.Application;
import ru.appril.kraspg.R;
import ru.appril.kraspg.activity.PassengersActivity;
import ru.appril.kraspg.activity.ScheduleActivity;
import ru.appril.kraspg.model.Search;
import ru.appril.kraspg.model.Station;
import ru.appril.kraspg.model.User;
import ru.appril.kraspg.network.RetrofitService;
import ru.appril.kraspg.ui.KpButton;
import ru.appril.kraspg.ui.KpCheck;
import ru.appril.kraspg.ui.KpDate;
import ru.appril.kraspg.ui.KpSelect;
import ru.appril.kraspg.ui.KpToolbar;
import ru.appril.kraspg.utils.Const;
import ru.appril.kraspg.utils.Prefs;

public class SearchFragment extends Fragment implements StationsFragment.OnStationListener,
        KpDate.OnDateListener, KpCheck.OnCheckListener, LoginFragment.OnLoginListener {
    private static final int BG[] = {R.drawable.bg_1, R.drawable.bg_2, R.drawable.bg_3,
            R.drawable.bg_4, R.drawable.bg_5, R.drawable.bg_6, R.drawable.bg_7};

    @BindView(R.id.search_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.search_check_search)
    TextView mCheckSearch;
    @BindView(R.id.search_check_schedule)
    TextView mCheckSchedule;
    @BindView(R.id.search_from)
    KpSelect mFrom;
    @BindView(R.id.search_to)
    KpSelect mTo;
    @BindView(R.id.search_swap)
    ImageView mSwap;
    @BindView(R.id.search_extra)
    LinearLayout mExtra;
    @BindView(R.id.search_check_bags)
    KpCheck mBags;
    @BindView(R.id.search_check_animals)
    KpCheck mAnimals;
    @BindView(R.id.search_date)
    KpDate mDate;
    @BindView(R.id.search_btn)
    KpButton mBtn;
    @BindView(R.id.search_bg)
    ImageView mBg;

    private int angle = 0;
    private boolean schedule = false;
    private Search search = new Search();

    private Prefs mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.search_title));
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        mBg.setImageResource(BG[new Random().nextInt(7)]);

        mDate.setOnDateListener(this);
        mBags.setOnCheckListener(this);
        mAnimals.setOnCheckListener(this);

        mPref = Prefs.init(getContext());
        Station from = mPref.getModel(Const.PREFS_SEARCH_FROM, Station.class);
        Station to = mPref.getModel(Const.PREFS_SEARCH_TO, Station.class);

        onStation(from, true);
        onStation(to, false);
        return view;
    }

    @Override
    public void onStation(Station station, boolean from) {
        if (from) {
            if (search.getFrom() != null && station.getStation_id() != search.getFrom().getStation_id())
                onStation(null, false);

            search.setFrom(station);
            mFrom.setText(station != null ? station.getStation() : "");
            mPref.setModel(Const.PREFS_SEARCH_FROM, station);
            enableBtn();
        } else {
            search.setTo(station);
            mTo.setText(station != null ? station.getStation() : "");
            mPref.setModel(Const.PREFS_SEARCH_TO, station);
            enableBtn();
        }
    }

    @Override
    public void onLogin(User user) {
        mPref.setValue(Const.PREFS_USER_ID, user.getUserId());
        mPref.setValue(Const.PREFS_TOKEN, user.getToken());
        mPref.setValue(Const.PREFS_EMAIL, user.getEmail());

        Application.getInstance().setUserProfile();
        RetrofitService.setToken(user.getToken());
        onClickBtn();
    }

    @Override
    public void onSelectedDate(String date) {
        search.setDate(date);
    }

    @Override
    public void onCheck(String text, int check) {
        switch (text) {
            case "Багаж":
                search.setBags(check);
                break;
            case "Животные":
                search.setAnimals(check);
                break;
        }
    }

    @OnClick(R.id.search_check_search)
    void onClickCheckSearch() {
        schedule = false;
        mCheckSearch.setTextColor(getResources().getColor(R.color.colorRed));
        mCheckSchedule.setTextColor(getResources().getColor(R.color.colorGray));
        mBags.setEnable();
        mAnimals.setEnable();
        mBtn.setText(getString(R.string.search_btn_search));
    }

    @OnClick(R.id.search_check_schedule)
    void onClickCheckSchedule() {
        schedule = true;
        mCheckSchedule.setTextColor(getResources().getColor(R.color.colorRed));
        mCheckSearch.setTextColor(getResources().getColor(R.color.colorGray));
        mBags.setDisable();
        mAnimals.setDisable();
        mBtn.setText(getString(R.string.search_btn_schedule));
    }

    @OnClick(R.id.search_from)
    void onClickStationFrom() {
        showStationsFragment(null);
    }

    @OnClick(R.id.search_to)
    void onClickStationTo() {
        if (search.getFrom() != null) {
            Bundle bundle = new Bundle();
            bundle.putInt("from", search.getFrom().getStation_id());
            bundle.putString("date", search.getDate());

            showStationsFragment(bundle);
        }
    }

    @OnClick(R.id.search_swap)
    void onClickSwap() {
        if (search.getFrom() != null && search.getTo() != null) {
            ViewAnimator
                    .animate(mSwap)
                    .rotation(angle += 180)
                    .duration(250)
                    .repeatMode(ViewAnimator.RESTART)
                    .start();

            search.swapStations();
            mFrom.setText(search.getFrom().getStation());
            mTo.setText(search.getTo().getStation());

            mPref.setModel(Const.PREFS_SEARCH_FROM, search.getFrom());
            mPref.setModel(Const.PREFS_SEARCH_TO, search.getTo());
            enableBtn();
        }
    }

    @OnClick(R.id.search_btn)
    void onClickBtn() {
        if (schedule)
            startActivity(new Intent(getContext(), ScheduleActivity.class)
                    .putExtra(Search.class.getName(), Parcels.wrap(search)));
        else {
            if (mPref.getValue(Const.PREFS_TOKEN, Const.PREFS_DEFAULT_STRING).isEmpty())
                showAuthFragment();
            else if (search.isFull()) {
                YandexMetrica.reportEvent("Поиск", new GsonBuilder().create().toJson(search));
                startActivity(new Intent(getContext(), PassengersActivity.class)
                        .putExtra(Search.class.getName(), Parcels.wrap(search)));
            }
        }
    }

    private void enableBtn() {
        mSwap.setEnabled(search.isFull());
        mBtn.setEnabled(search.isFull());
    }

    private void showStationsFragment(Bundle bundle) {
        StationsFragment stationsFragment = new StationsFragment();
        stationsFragment.setOnStationListener(this);

        if (bundle != null)
            stationsFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        stationsFragment.show(fragmentTransaction, StationsFragment.TAG);
    }

    private void showAuthFragment() {
        LoginFragment authFragment = new LoginFragment();
        authFragment.setOnLoginListener(this);

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        authFragment.show(fragmentTransaction, LoginFragment.TAG);
    }
}