package ru.appril.kraspg.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.yandex.metrica.YandexMetrica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import butterknife.BindArray;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ru.appril.kraspg.R;
import ru.appril.kraspg.contact.PassengerContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Passenger;
import ru.appril.kraspg.presenter.PassengerPresenter;
import ru.appril.kraspg.ui.KpButton;
import ru.appril.kraspg.ui.KpToolbar;

public class PassengerFragment extends DialogFragment implements PassengerContact.View,
        KpToolbar.OnNavigationListener {
    public static final String TAG = "PassengerFragment";

    @BindView(R.id.passenger_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.passenger_full_name)
    EditText mFullName;
    @BindView(R.id.passenger_document_type)
    TextView mDocumentType;
    @BindView(R.id.passenger_document_number_passport)
    EditText mDocumentNumberPassport;
    @BindView(R.id.passenger_document_number_drivers)
    EditText mDocumentNumberDrivers;
    @BindView(R.id.passenger_document_number_certificate)
    EditText mDocumentNumberCertificate;
    @BindView(R.id.passenger_progress)
    ProgressBar mProgress;
    @BindView(R.id.passenger_btn_next)
    KpButton mBtnNext;
    @BindArray(R.array.passenger_document_types)
    String[] TYPES;
    @BindColor(R.color.colorBlack)
    int colorBlack;

    private String full_name, document_number;
    private boolean f = false, n = false;

    private PassengerPresenter mPassengerPresenter;
    private OnPassengerListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenFragment_Base);
        YandexMetrica.reportEvent(getString(R.string.passenger_title));
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_passenger, container, false);
        ButterKnife.bind(this, view);

        MaskedTextChangedListener.Companion.installOn(
                mDocumentNumberPassport, "[0000]{ }[000000]",
                (b, s) -> {
                    n = b;
                    if (b) document_number = s;
                    setBtnEnable();
                }
        );

        MaskedTextChangedListener.Companion.installOn(
                mDocumentNumberDrivers, "[00]{ }[00]{ }[000000]",
                (b, s) -> {
                    n = b;
                    if (b) document_number = s;
                    setBtnEnable();
                }
        );

        mToolbar.setOnNavigationListener(this);
        mBtnNext.setEnabled(false);
        mPassengerPresenter = new PassengerPresenter(this);
        return view;
    }

    @Override
    public void onClickNavigation() {
        dismiss();
    }

    @Override
    public void showPassenger(Passenger passenger) {
        mListener.onPassenger(passenger);
        dismiss();
    }

    @Override
    public void showError(Error error) {
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setEnable(boolean enable) {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                mFullName.setEnabled(enable);
                mDocumentType.setEnabled(enable);
                mDocumentNumberPassport.setEnabled(enable);
                mDocumentNumberDrivers.setEnabled(enable);
                mDocumentNumberCertificate.setEnabled(enable);
                mProgress.setVisibility(enable ? View.GONE : View.VISIBLE);
                mBtnNext.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
            });
    }

    @OnTextChanged(R.id.passenger_full_name)
    void onTextChangedFullName(CharSequence s) {
        String[] a = s.toString().trim().split(" ");
        boolean b = a.length == 3;

        f = b;
        if (b) full_name = s.toString().trim();
        setBtnEnable();
    }

    @OnTextChanged(R.id.passenger_document_number_certificate)
    void onTextChangedDocumentNumberCertificate(CharSequence s) {
        n = s.length() > 9;
        if (n) document_number = s.toString();
        setBtnEnable();
    }

    @OnClick(R.id.passenger_document_type)
    void onClickType() {
        if (getContext() != null)
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.passenger_document_type)
                    .setItems(R.array.passenger_document_types, (dialog, which) -> {
                        mDocumentNumberPassport.getText().clear();
                        mDocumentNumberDrivers.getText().clear();
                        mDocumentNumberCertificate.getText().clear();
                        mDocumentType.setText(TYPES[which]);
                        mDocumentType.setTextColor(colorBlack);

                        switch (which) {
                            case 0:
                                mDocumentNumberPassport.setVisibility(View.VISIBLE);
                                mDocumentNumberDrivers.setVisibility(View.GONE);
                                mDocumentNumberCertificate.setVisibility(View.GONE);
                                break;
                            case 1:
                                mDocumentNumberPassport.setVisibility(View.GONE);
                                mDocumentNumberDrivers.setVisibility(View.VISIBLE);
                                mDocumentNumberCertificate.setVisibility(View.GONE);
                                break;
                            case 2:
                                mDocumentNumberPassport.setVisibility(View.GONE);
                                mDocumentNumberDrivers.setVisibility(View.GONE);
                                mDocumentNumberCertificate.setVisibility(View.VISIBLE);
                                break;
                        }
                    })
                    .create()
                    .show();
    }

    @OnClick(R.id.passenger_btn_next)
    void onClickNext() {
        mPassengerPresenter.addPassenger(full_name, mDocumentType.getText().toString(), document_number);
    }

    private void setBtnEnable() {
        mBtnNext.setEnabled(f && n);
    }

    public void setOnPassengerListener(OnPassengerListener listener) {
        mListener = listener;
    }

    public interface OnPassengerListener {
        void onPassenger(Passenger passenger);
    }
}