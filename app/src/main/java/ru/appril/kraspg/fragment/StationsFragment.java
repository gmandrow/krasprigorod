package ru.appril.kraspg.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.yandex.metrica.YandexMetrica;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;
import ru.appril.kraspg.adapter.StationAdapter;
import ru.appril.kraspg.contact.StationContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.Station;
import ru.appril.kraspg.presenter.StationPresenter;
import ru.appril.kraspg.ui.KpErrorView;
import ru.appril.kraspg.ui.KpToolbar;

public class StationsFragment extends DialogFragment implements StationContact.View,
        StationAdapter.OnItemClickListener, KpToolbar.OnNavigationListener, KpToolbar.OnSearchListener {
    static final String TAG = "StationsFragment";

    @BindView(R.id.stations_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.stations_recycler)
    RecyclerView mRecycler;
    @BindView(R.id.stations_progress)
    ProgressBar mProgress;
    @BindView(R.id.stations_error)
    KpErrorView mError;

    private List<Station> stations = new ArrayList<>();
    private String date;
    private int from;

    private StationAdapter mAdapter;
    private OnStationListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenFragment_Base);
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            YandexMetrica.reportEvent("Откуда");
            Bundle bundle = getArguments();

            from = bundle.getInt("from", 0);
            date = bundle.getString("date");
        } else
            YandexMetrica.reportEvent("Куда");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_stations, container, false);
        ButterKnife.bind(this, view);

        mToolbar.setOnNavigationListener(this);
        mToolbar.setOnSearchListener(this);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setHasFixedSize(false);

        mAdapter = new StationAdapter(stations, this);
        mRecycler.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        if (dialog != null && dialog.getWindow() != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;

            dialog.getWindow().setLayout(width, height);
        }

        StationPresenter mStationPresenter = new StationPresenter(this);
        if (from == 0) {
            mStationPresenter.getStationsFromDb();
            mStationPresenter.getStationsFrom();
        } else
            mStationPresenter.getStationsTo(from, date);
    }

    @Override
    public void onClickNavigation() {
        dismiss();
    }

    @Override
    public void onSearch(String search) {
        mAdapter.getFilter().filter(search);
    }

    @Override
    public void onItemClick(Station station) {
        mListener.onStation(station, from == 0);
        dismiss();
    }

    @Override
    public void showStations(List<Station> stations) {
        this.stations.clear();
        this.stations.addAll(stations);

        if (getActivity() != null)
            getActivity().runOnUiThread(() -> mAdapter.notifyDataSetChanged());
    }

    @Override
    public void showProgress() {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> mProgress.setVisibility(View.VISIBLE));
    }

    @Override
    public void hideProgress() {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> mProgress.setVisibility(View.GONE));
    }

    @Override
    public void showError(Error error) {
        if (getActivity() != null && stations.size() == 0)
            getActivity().runOnUiThread(() -> mError.setTitle(error.getMessage()).show());
    }

    @Override
    public void hideError() {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> mError.hide());
    }

    void setOnStationListener(OnStationListener listener) {
        mListener = listener;
    }

    public interface OnStationListener {
        void onStation(Station station, boolean from);
    }
}