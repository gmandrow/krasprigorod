package ru.appril.kraspg.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.yandex.metrica.YandexMetrica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ru.appril.kraspg.R;
import ru.appril.kraspg.activity.TextActivity;
import ru.appril.kraspg.contact.LoginContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.model.User;
import ru.appril.kraspg.presenter.LoginPresenter;
import ru.appril.kraspg.ui.KpButton;
import ru.appril.kraspg.ui.KpToolbar;

public class LoginFragment extends DialogFragment implements LoginContact.View, KpToolbar.OnNavigationListener {
    public static final String TAG = "LoginFragment";

    @BindView(R.id.login_toolbar)
    KpToolbar mToolbar;
    @BindView(R.id.login_email)
    EditText mEmail;
    @BindView(R.id.login_password)
    EditText mPassword;
    @BindView(R.id.login_progress)
    ProgressBar mProgress;
    @BindView(R.id.login_btn_main)
    KpButton mBtnMain;
    @BindView(R.id.login_btn_delete_password)
    TextView mBtnDeletePassword;
    @BindView(R.id.login_btn_secondary)
    TextView mBtnSecondary;

    private boolean e = false, p = false, c = true, auth = true;

    private OnLoginListener mListener;
    private LoginPresenter mLoginPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenFragment_Base);
        YandexMetrica.reportEvent(getString(R.string.login_title_auth));
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        mToolbar.setOnNavigationListener(this);
        mBtnMain.setEnabled(false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        if (dialog != null && dialog.getWindow() != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;

            dialog.getWindow().setLayout(width, height);
        }

        mLoginPresenter = new LoginPresenter(this);
    }

    @OnCheckedChanged(R.id.login_btn_check)
    void onCheck(boolean check) {
        c = check;
        setBtnEnable();
    }

    @OnClick(R.id.login_btn_main)
    void onClickAuth() {
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        String device = "Android";

        if (auth)
            mLoginPresenter.Auth(email, password, device);
        else
            mLoginPresenter.Reg(email, password, device);
    }

    @OnClick(R.id.login_btn_secondary)
    void onClickReg() {
        auth = !auth;
        mToolbar.setTitle(auth ? getString(R.string.login_title_auth) : getString(R.string.login_title_reg));
        mBtnMain.setText(auth ? getString(R.string.login_btn_auth) : getString(R.string.login_btn_reg));
        mBtnDeletePassword.setVisibility(auth && e ? View.VISIBLE : View.GONE);
        mBtnSecondary.setText(auth ? getString(R.string.login_btn_secondary_reg) : getString(R.string.login_btn_secondary_auth));
        YandexMetrica.reportEvent(auth ? getString(R.string.login_title_auth) : getString(R.string.login_title_reg));
    }

    @OnClick(R.id.login_btn_delete_password)
    void onClickDeletePassword() {
        mLoginPresenter.deletePassword(mEmail.getText().toString());
    }

    @OnClick(R.id.login_btn_confidential)
    void onClickConfidential() {
        startActivity(new Intent(getContext(), TextActivity.class)
                .putExtra("state", "confidential"));
    }

    @OnClick(R.id.login_btn_rules)
    void onClickRules() {
        startActivity(new Intent(getContext(), TextActivity.class)
                .putExtra("state", "rules"));
    }

    @OnClick(R.id.login_btn_regulations)
    void onClickRegulations() {
        startActivity(new Intent(getContext(), TextActivity.class)
                .putExtra("state", "regulations"));
    }

    @OnTextChanged(R.id.login_email)
    void onTextChangedEmail(CharSequence s) {
        e = Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches();
        mBtnDeletePassword.setVisibility(auth && e ? View.VISIBLE : View.GONE);
        setBtnEnable();
    }

    @OnTextChanged(R.id.login_password)
    void onTextChangedPassword(CharSequence s) {
        p = s.length() > 3;
        setBtnEnable();
    }

    @Override
    public void Auth(User user) {
        mListener.onLogin(user);
        dismiss();
    }

    @Override
    public void Reg(User user) {
        mListener.onLogin(user);
        dismiss();
    }

    @Override
    public void showPassword(String email) {
        if (getContext() != null)
            new AlertDialog.Builder(getContext())
                    .setTitle("Восстановление пароля")
                    .setMessage("Новый пароль отправлен на почту: " + email)
                    .setPositiveButton(getString(android.R.string.ok),
                            (dialog, which) -> dialog.dismiss()).create()
                    .show();
    }

    @Override
    public void showError(Error error) {
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setEnable(boolean enable) {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                mEmail.setEnabled(enable);
                mPassword.setEnabled(enable);
                mBtnSecondary.setEnabled(enable);
                mProgress.setVisibility(enable ? View.GONE : View.VISIBLE);
                mBtnMain.setVisibility(enable ? View.VISIBLE : View.INVISIBLE);
            });
    }

    @Override
    public void onClickNavigation() {
        dismiss();
    }

    private void setBtnEnable() {
        mBtnMain.setEnabled(e && p && c);
    }

    void setOnLoginListener(OnLoginListener listener) {
        mListener = listener;
    }

    public interface OnLoginListener {
        void onLogin(User user);
    }
}