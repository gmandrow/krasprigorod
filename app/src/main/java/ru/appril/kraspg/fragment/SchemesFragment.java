package ru.appril.kraspg.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.yandex.metrica.YandexMetrica;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.appril.kraspg.R;

public class SchemesFragment extends Fragment {
    @BindView(R.id.schemes_tab)
    TabLayout mTab;
    @BindView(R.id.schemes_viewpager)
    ViewPager mViewPager;
    @BindArray(R.array.schemes_names)
    String[] names;
    @BindArray(R.array.schemes_urls)
    String[] urls;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YandexMetrica.reportEvent(getString(R.string.schemes_title));
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schemes, container, false);
        ButterKnife.bind(this, view);

        mViewPager.setAdapter(new TabAdapter(getChildFragmentManager()));
        mViewPager.setOffscreenPageLimit(1);

        mTab.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(1);
        return view;
    }

    private class TabAdapter extends FragmentPagerAdapter {
        TabAdapter(FragmentManager mgr) {
            super(mgr);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return names[position];
        }

        @Override
        public int getCount() {
            return urls.length;
        }

        @Override
        public Fragment getItem(int position) {
            SchemeFragment schemeFragment = new SchemeFragment();

            Bundle bundle = new Bundle();
            bundle.putString("url", urls[position]);
            schemeFragment.setArguments(bundle);

            return schemeFragment;
        }
    }
}