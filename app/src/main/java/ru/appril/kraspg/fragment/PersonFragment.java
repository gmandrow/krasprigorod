package ru.appril.kraspg.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import ru.appril.kraspg.R;
import ru.appril.kraspg.contact.PersonContact;
import ru.appril.kraspg.model.Error;
import ru.appril.kraspg.presenter.PersonPresenter;
import ru.appril.kraspg.ui.KpButton;
import ru.appril.kraspg.utils.Const;
import ru.appril.kraspg.utils.Prefs;

public class PersonFragment extends Fragment implements PersonContact.View {
    @BindView(R.id.personal_email)
    TextView mEmail;
    @BindView(R.id.personal_old_password)
    EditText mEditOldPassword;
    @BindView(R.id.personal_new_password2)
    EditText mEditNewPassword;
    @BindView(R.id.personal_progress)
    ProgressBar mProgress;
    @BindView(R.id.personal_btn_change_password)
    KpButton mBtnChangePassword;
    @BindView(R.id.personal_btn_password)
    TextView mBtnPassword;

    private boolean n = false, o = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal, container, false);
        ButterKnife.bind(this, view);
        Prefs mPref = Prefs.init(getContext());

        if (!mPref.getValue(Const.PREFS_TOKEN, Const.PREFS_DEFAULT_STRING).isEmpty()) {
            mEmail.setVisibility(View.VISIBLE);
            mEmail.setText(mPref.getValue(Const.PREFS_EMAIL, Const.PREFS_DEFAULT_STRING));
            mBtnPassword.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @OnClick(R.id.personal_btn_change_password)
    void onClickChangePassword() {
        mEditOldPassword.setEnabled(false);
        mEditNewPassword.setEnabled(false);
        mProgress.setVisibility(View.VISIBLE);
        mBtnPassword.setVisibility(View.INVISIBLE);

        PersonPresenter mPersonPresenter = new PersonPresenter(this);
        mPersonPresenter.changePassword(mEditOldPassword.getText().toString(), mEditNewPassword.getText().toString());
    }

    @OnClick(R.id.personal_btn_password)
    void onClickPassword() {
        mEditOldPassword.setVisibility(View.VISIBLE);
        mEditNewPassword.setVisibility(View.VISIBLE);
        mBtnChangePassword.setVisibility(View.VISIBLE);
        mBtnPassword.setVisibility(View.GONE);
        mBtnChangePassword.setEnabled(false);
    }

    @OnTextChanged(R.id.personal_old_password)
    void onTextChangedOldPassword(CharSequence s) {
        o = s.length() > 3;
        setBtnEnable();
    }

    @OnTextChanged(R.id.personal_new_password2)
    void onTextChangedNewPassword(CharSequence s) {
        n = s.length() > 3;
        setBtnEnable();
    }

    @Override
    public void changePassword() {
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                mEditOldPassword.getText().clear();
                mEditOldPassword.setVisibility(View.GONE);
                mEditNewPassword.getText().clear();
                mEditNewPassword.setVisibility(View.GONE);
                mProgress.setVisibility(View.GONE);
                mBtnChangePassword.setVisibility(View.GONE);
                mBtnPassword.setVisibility(View.VISIBLE);
            });

        Toast.makeText(getContext(), "Пароль изменен", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(Error error) {
        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void setBtnEnable() {
        mBtnChangePassword.setEnabled(n && o);
    }
}